import React from 'react'
import { Route, Switch } from "react-router-dom"

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Login Components
const IndexUsers   = asyncComponent(() => import('../../Components/Pages/Users/indexUsers/IndexUsers'))
const ShowUser     = asyncComponent(() => import('../../Components/Pages/Users/showUser/ShowUser'))
const EditUser     = asyncComponent(() => import('../../Components/Pages/Users/editUser/EditUser'))
const AddUser     = asyncComponent(() => import('../../Components/Pages/Users/addUser/AddUser'))
//import IndexUsers from '../../Components/Pages/Users/indexUsers/IndexUsers'
//import ShowUser from '../../Components/Pages/Users/showUser/ShowUser'
//import EditUser from '../../Components/Pages/Users/editUser/EditUser'


const usersRouters = (
    <Switch>
      <Route path="/users" exact component={IndexUsers} />
      <Route path="/users/edit/:id" exact component={EditUser} />
      <Route path="/users/show/:id" exact component={ShowUser} />
      <Route path="/users/add" exact component={AddUser} />
    </Switch>
)
export default usersRouters
