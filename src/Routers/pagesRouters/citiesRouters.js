import React from 'react'
import { Route, Switch } from "react-router-dom"

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Cities Components
const IndexCities   = asyncComponent(() => import('../../Components/Pages/Cities/indexCities/IndexCities'))
const AddCity   = asyncComponent(() => import('../../Components/Pages/Cities/addCity/AddCity'))
const EditCity   = asyncComponent(() => import('../../Components/Pages/Cities/editCity/EditCity'))



const citiesRouters = (
    <Switch>
      <Route path="/cities" exact component={IndexCities} />
      <Route path="/cities/add" exact component={AddCity} />
      <Route path="/cities/edit/:id" exact component={EditCity} />
    </Switch>
)
export default citiesRouters
