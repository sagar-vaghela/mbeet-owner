import React from 'react'
import { Route, Switch } from "react-router-dom"

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Login Components
const IndexBookings   = asyncComponent(() => import('../../Components/Pages/Bookings/indexBooking/IndexBooking'))



const BookingsRouters = (
    <Switch>
      <Route path="/bookings" exact component={IndexBookings} />
    </Switch>
)
export default BookingsRouters
