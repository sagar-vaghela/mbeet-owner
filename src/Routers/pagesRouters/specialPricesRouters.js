import React from 'react'
import { Route, Switch } from "react-router-dom"

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Login Components
const IndexSpecialPrices   = asyncComponent(() => import('../../Components/Pages/Units/SpecialPrices/indexSpecialPrices/IndexSpecialPrices'))
const AddSpecialPrice   = asyncComponent(() => import('../../Components/Pages/Units/SpecialPrices/addSpecialPrice/AddSpecialPrice'))
const EditSpecialPrice   = asyncComponent(() => import('../../Components/Pages/Units/SpecialPrices/editSpecialPrice/EditSpecialPrice'))

const specialPricesRouters = (
    <Switch>
        <Route path="/units/:id/special_price" exact component={IndexSpecialPrices} />
        <Route path="/units/:id/special_price/add" exact component={AddSpecialPrice} />
        <Route path="/units/:unit_id/special_price/edit/:id" exact component={EditSpecialPrice} />
    </Switch>
)
export default specialPricesRouters
