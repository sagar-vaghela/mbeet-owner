import React from 'react'
import { Route, Switch } from "react-router-dom"
import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Dashboard Components
const Dashboard     = asyncComponent(() => import('../../Components/Pages/Dashboard/Dashboard'))


const dashboardRouters = (
  <Switch>
    <Route exact path="/" component={Dashboard} />
  </Switch>
)
export default dashboardRouters
