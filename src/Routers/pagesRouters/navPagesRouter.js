import React from 'react'
import { Route, Switch } from "react-router-dom"

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Login Components
const About           = asyncComponent(() => import('../../Components/Pages/About/About'))
const Privacy         = asyncComponent(() => import('../../Components/Pages/Privacy/Privacy'))
const ContactUs       = asyncComponent(() => import('../../Components/Pages/ContactUs/ContactUs'))
const Home            = asyncComponent(() => import('../../Components/Pages/Home/Home'))
const Login           = asyncComponent(() => import('../../Components/Pages/Auth/login/Login'))
const Registration    = asyncComponent(() => import('../../Components/Pages/Auth/registration/Registration'))
const ResetPassword   = asyncComponent(() => import('../../Components/Pages/Auth/resetPassword/ResetPassword'))



const navPagesRouter = (
  <Switch>
    <Route path="/home" component={Home} />
    <Route path="/login" component={Login} />
    <Route path="/registration" component={Registration} />
    <Route path="/reset_password" component={ResetPassword} />
    <Route path="/about" component={About} />
    <Route path="/privacy" component={Privacy} />
    <Route path="/contact_us" component={ContactUs} />
  </Switch>
)
export default navPagesRouter
