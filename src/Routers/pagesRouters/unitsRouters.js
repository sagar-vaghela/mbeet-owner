import React from 'react'
import { Route, Switch } from "react-router-dom"

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Login Components
const IndexUnits   = asyncComponent(() => import('../../Components/Pages/Units/indexUnits/IndexUnits'))
const AddUnit   = asyncComponent(() => import('../../Components/Pages/Units/addUnit/AddUnit'))
const EditUnit   = asyncComponent(() => import('../../Components/Pages/Units/editUnit/EditUnit'))
const ShowUnit   = asyncComponent(() => import('../../Components/Pages/Units/showUnit/ShowUnit'))




const unitsRouters = (
    <Switch>
      <Route path="/units" exact component={IndexUnits} />
      <Route path="/units/add" exact component={AddUnit} />
      <Route path="/units/edit/:id" exact component={EditUnit} />
      <Route path="/units/show/:id" exact component={ShowUnit} />
    </Switch>
)
export default unitsRouters
