import React from 'react'
import { Redirect } from 'react-router'

import navPagesRouter from './pagesRouters/navPagesRouter'
import AuthPagesRouters from './AuthPagesRouters'
import Navbar from '../Components/Header/Navbar/Navbar'

const RootRouters = (props) => {
  const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false
  return (
    <div className="container-fluid h-100 clearfix">
      <Navbar />

      {isLogged ? (
        <AuthPagesRouters />
      ):(
        <div>
          <Redirect to="/home" />
          {navPagesRouter}
        </div>

      )}

    </div>
  )
}

export default RootRouters
