import React from 'react'

// Authenticated Pages Routers
import dashboardRouters from './pagesRouters/dashboardRouters'
// import usersRouters from './pagesRouters/usersRouters'
import unitsRouters from './pagesRouters/unitsRouters'
import bookingsRouters from './pagesRouters/bookingsRouters'
// import citiesRouters from './pagesRouters/citiesRouters'
import specialPricesRouters from './pagesRouters/specialPricesRouters'

import Sidebar from '../Components/Sidebar/Sidebar'

const AuthPagesRouters = () => (
  <div className="row clearfix">
    <div className="h-100 col-lg-2 col-md-2">
      <Sidebar />
    </div>

    {dashboardRouters}
    {bookingsRouters}
    {specialPricesRouters}
    {unitsRouters}


  </div>
)
export default AuthPagesRouters
