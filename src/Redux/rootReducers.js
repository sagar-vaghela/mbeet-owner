import  {combineReducers} from  'redux'

// Reducers
import authReducer from './reducers/authReducer'
import setLocaleReducer from './reducers/setLocaleReducer'
import dashboardReducer from './reducers/dashboardReducer'
import bookingsReducer from './reducers/bookingsReducer'
import unitsReducer from './reducers/unitsReducer'
import specialPricesReducer from './reducers/specialPricesReducer'
import citiesReducer from './reducers/citiesReducer'
import usersReducer from './reducers/usersReducer'


const rootReducers = combineReducers({
  auth: authReducer,
  setLocale: setLocaleReducer,
  dashboard: dashboardReducer,
  bookings: bookingsReducer,
  units: unitsReducer,
  specialPrices: specialPricesReducer,
  cities: citiesReducer,
  users: usersReducer,
});


export default rootReducers;
