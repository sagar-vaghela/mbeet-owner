/****************
*   Auth Actions
*
*   @LOGIN_REQUEST
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
* login - login owner
*
* @userData : (Object) has {email, password}
*/
export function login(userData){

  const url     = '/'+API_VERSION+'/login_owner'
  const method  = 'POST'
  const locale  = localStorage.getItem('LOCALE')

  userData = {...userData, locale: locale}

  return {
    type: 'LOGIN_REQUEST',
    payload: handleAPI(url, method, userData)
  }
}

// On Success Login
export function onSuccessLogin(data){
  sessionStorage.setItem('TOKEN', data.auth_token)
  sessionStorage.setItem('USERNAME', data.users.name)
  sessionStorage.setItem('USERID', data.users.id)
  if(data.users.image)
    sessionStorage.setItem('USER_IMG', data.users.image.name)
}

/*
* registration - registration owner
*
* @userData : (Object) has {email, password}
*/

export function registration(userData){

  const url     = '/'+API_VERSION+'/owner_signup'
  const method  = 'POST'
  const locale  = localStorage.getItem('LOCALE')

  userData = {...userData, locale: locale}

  return {
    type: 'REGISTRATION_REQUEST',
    payload: handleAPI(url, method, userData)
  }
}

export function contactUs(userData){

  const url     = '/'+API_VERSION+'/contact_us'
  const method  = 'POST'
  const locale  = localStorage.getItem('LOCALE')

  userData = {...userData, locale: locale}

  return {
    type: 'CONTACTUS_REQUEST',
    payload: handleAPI(url, method, userData)
  }
}


// On Success Registration
export function onSuccessRegistration(data){
  sessionStorage.setItem('TOKEN', data.auth_token)
  sessionStorage.setItem('USERNAME', data.users.name)
  sessionStorage.setItem('USERID', data.users.id)
  if(data.users.image)
    sessionStorage.setItem('USER_IMG', data.users.image.name)
}


export function resetPassword(email){

  const url     = '/'+API_VERSION+'/forgot_password?email='+email
  const method  = 'GET'
  const locale  = localStorage.getItem('LOCALE')


  return {
    type: 'RESET_PASSWORD',
    payload: handleAPI(url, method)
  }

}

export function initAuth(){
  return {
    type: 'INIT_AUTH',
    payload: new Promise((resolve, reject) => resolve(1))
  }
}
