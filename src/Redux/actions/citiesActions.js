/****************
*   Cities Actions
*
*   @GET_CITIES
*   @GET_CITY
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getCities - fetch Cities
*/
export function getCities(limit = 10, offset = 0, fltr = null, sort_by='id', sort_direction='asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/cities?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search

  if(fltr)
    url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_CITIES',
    payload: handleAPI(url, method, false, Authorization)
  }

}
/*
*   GetCity
*
*   @cityid
*/
export function getCity(cityid){

  const url           = '/admin/'+API_VERSION+'/cities/'+cityid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'GET_CITY',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   Add City
*
*   @dataForm
*/
export function addCity(data){

  const url           = '/admin/'+API_VERSION+'/cities'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_CITY',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/*
*   Edit City
*
*   @cityid
*   @dataForm
*/
export function editCity(cityid, data){

  const url           = '/admin/'+API_VERSION+'/cities/'+cityid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'EDIT_CITY',
    payload: handleAPI(url, method, data, Authorization)
  }
}

/***************************
* On Click Delete
* @userid : (Intger)
****************************/
export function deleteCity(cityid){

  const url           = '/admin/'+API_VERSION+'/cities/'+cityid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_CITY',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(cityid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}

/*******************
* Users Search
*
* @s: (String)
********************/
export function citiesSearch(s){

  let url           = '/admin/'+API_VERSION+'/cities?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_CITIES',
    payload: handleAPI(url, method, false, Authorization)
  }

}
