/****************
*   Bookings Actions
*
*   @GET_Bookings
*   @GET_Booking
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getUsers - fetch users
*/
export function getBookings(limit = 10, offset = 0, fltr = null, sort_by='created_at', sort_direction='desc', search = '', dateFilter = null){

  let url             = '/'+API_VERSION+'/my_orders/owner_order?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}
  const UserId        = sessionStorage.getItem('USERID')

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search

  if(fltr)
    url += '&'+fltr.params+'='+fltr.value

  if(dateFilter){
    if(dateFilter.from && dateFilter.to)
      url += `&type=${dateFilter.type}&from=${dateFilter.from}&to=${dateFilter.to}`
  }

  if (UserId)
  {
    url += '&user_id='+UserId
  }

  return {
    type: 'GET_BOOKINGS',
    payload: handleAPI(url, method, false, Authorization)
  }

}
