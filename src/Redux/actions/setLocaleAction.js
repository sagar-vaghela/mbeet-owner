export function setLocale(LOCALE){
  localStorage.LOCALE = LOCALE;
  return {
    type: 'SET_LOCALE',
    LOCALE: localStorage.LOCALE
  }
}
