/****************
*   SpecialPrices Actions
*
*   @GET_SPECIALPRICES
*   @GET_SPECIALPRICE
*   @ADD_SPECIALPRICE
*   @DELETE_SPECIALPRICE
*   @UPDATE_SPECIALPRICE
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getSpecialPrices - fetch specialPrices
*/
// export function getSpecialPrices(limit = 10, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = '', unit_id ){
export function getSpecialPrices(limit = 10, offset = 0, sort_by = 'id', sort_direction = 'asc', search = '', unit_id ){

  let url           = '/'+API_VERSION+'/periods?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search+'&unit_id='+unit_id

  // if(fltr)
  //   url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_SPECIALPRICES',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   Get SpecialPrice
*
*   @specialPriceid
*/
export function getSpecialPrice(specialPriceid){

  const url           = '/'+API_VERSION+'/periods/'+specialPriceid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}



  return {
    type: 'GET_SPECIALPRICE',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   GET SpecialPrice
*
*   @specialPriceid: (Intger)
*/

/*
*   GET SpecialPrice
*
*   @specialPriceid: (Intger)
*/
export function addSpecialPrice(data){

  const url           = '/'+API_VERSION+'/periods/multiple_create'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_SPECIALPRICE',
    payload: handleAPI(url, method, data, Authorization)
  }

}


/*
*   Delete SpecialPrice
*
*   @specialPriceid: (Intger)
*/
export function deleteSpecialPrice(specialPriceid){

  const url           = '/'+API_VERSION+'/periods/'+specialPriceid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_SPECIALPRICE',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(specialPriceid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}

// export function restoreSpecialPrice(specialPriceid){
//
//   const url           = '/admin/'+API_VERSION+'/specialPrice_restores/'+specialPriceid
//   const method        = 'PUT'
//   const locale        = localStorage.getItem('LOCALE')
//   const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}
//
//   return {
//     type: 'RESTORE_SPECIALPRICE',
//     payload: new Promise((resolve, reject) => {
//       handleAPI(url, method, false, Authorization).then(() => {
//         resolve(specialPriceid)
//       }).catch(err => {
//         reject(err)
//       })
//     })
//   }
//
// }




/*
*   EDIT SpecialPrice
*
*   @specialPriceid: (Intger)
*   @data: fromData
*/
export function editSpecialPrice(specialPriceid, data){

  const url           = '/'+API_VERSION+'/periods/'+specialPriceid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'EDIT_SPECIALPRICE',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/*******************
* SpecialPrices Search
*
* @s: (String)
********************/
export function specialPricesSearch(s){

  let url           = '/'+API_VERSION+'/specialPrices?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_SPECIALPRICES',
    payload: handleAPI(url, method, false, Authorization)
  }

}
