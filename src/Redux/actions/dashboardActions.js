/****************
*   Bookings Actions
*
*   @GET_Bookings
*   @GET_Booking
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

export function getRegisteredUsers(date = null){
  const action = 'GET_REGISTERED_USERS'+(date ? '_TODAY' : '')
  const method = 'GET'
  let url      = '/admin/'+API_VERSION+'/users/dashbord_users'
  url         += (date) ? `?date=${date}` : ''

  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: action,
    payload: handleAPI(url, method, false, Authorization)
  }
}

export function getTotalBookings(date = null, type = null, current_all = false){
  // const action = 'GET_TOTAL_BOOKINGS'+(date ? '_TODAY' : '')
  const action = 'GET_TOTAL_BOOKINGS'+(date ? '_TODAY' : current_all == true ? '_LIFETIME' : '')
  const method = 'GET'
  let url      = '/'+API_VERSION+'/owners_dashboard'
  url         += (date) ? `?date=${date}` : ''
  url         += (type) ? `&type=${type}` : ''
  url         += (current_all) ? `?current_all=${current_all}` : ''

  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: action,
    payload: handleAPI(url, method, false, Authorization)
  }
}
