/*
* Login Reducer
*/

const DEFAULT_STATE = {
  LOCALE: ''
}

let changes = null
export default function setLocaleReducer (state = DEFAULT_STATE, action) {
  switch (action.type){

    case "SET_LOCALE":
    changes = {
      LOCALE: action.LOCALE,
    }
    return {...state, ...changes}
  }
  return state
}
