/*
* User Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  users: null,
  user: null,
  user_units: null,
  totalUsers: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
  fetchingUnits: false,
}

let changes = null
export default function usersReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_USERS_PENDING":
    case "GET_USER_PENDING":
    case "ADD_USER_PENDING":
    case "DELETE_USER_PENDING":
    case "EDIT_USER_PENDING":
    case "SEARCH_USERS_PENDING":
    case "FILTER_USERS_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, user: null}


    /*************************
    *   Index Users
    **************************/
    /*
    * On Failed
    * Action: put error message on the state
    */
    case "GET_USERS_REJECTED":
    case "FILTER_USERS_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*
    * On Success
    * Action: put the users on the state
    */
    case "GET_USERS_FULFILLED":
    case "FILTER_USERS_FULFILLED":
      changes = {
        users: action.payload.data.data.users,
        totalUsers: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }

      return {...state, ...changes}

    // On Delete
    case "DELETE_USER_FULFILLED":
    case "RESTORE_USER_FULFILLED":
      changes = {
        users : state.users.filter(user => user.id !== action.payload),
        fetching: false,
        errMsg: null,
        succMsg: null,
      }
      return {...state, ...changes}

    // On failed delete
    case "DELETE_USER_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Show User
    **************************/

    // On success get user
    case "GET_USER_FULFILLED":
      changes = {
        user: action.payload.data.data.users,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On failed get user
    case "GET_USER_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   User Units
    **************************/
    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_USER_UNITS_PENDING":
      return {...state, fetchingUnits: true}

    case "GET_USER_UNITS_FULFILLED":
      changes = {
        user_units: action.payload.data.data.units,
        errMsg: null,
        fetchingUnits: false,
      }
      return {...state, ...changes}

    /*************************
    *   Add User
    **************************/
    // On Success Add user
    case "ADD_USER_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "ADD_USER_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Edit User
    **************************/
    // On Success
    case "EDIT_USER_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "EDIT_USER_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Search Users
    **************************/
    case "SEARCH_USERS_FULFILLED":
      changes = {
        users: action.payload.data.data.users,
        totalUsers: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Delete Unit
    **************************/
    // On Success
    case "DELETE_USER_UNIT_FULFILLED":
      state.user_units.map(unit => {
        if(unit.id === action.payload)
          unit.soft_delete = true
      })
      return state

    /*************************
    *   Restore Unit
    **************************/
    // On Success
    case "RESTORE_USER_UNIT_FULFILLED":
      state.user_units.map(unit => {
        if(unit.id === action.payload)
          unit.soft_delete = false
      })
      changes = {
        fetching: false,
      }
      return {...state, ...changes}

  } // / End Switch

  return state

}
