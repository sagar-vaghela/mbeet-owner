/*
* Booking Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  total_life_time_bookings: {
    data: null,
    errMsg: null,
  },
  total_bookings: {
    data: null,
    errMsg: null,
  },
  today_bookings: {
    data: null,
    errMsg: null,
  }
}

export default function dashboardReducer (state = DEFAULT_STATE, action) {
  switch (action.type){

    /*****************
    * Total Bookings
    ******************/

    case 'GET_TOTAL_BOOKINGS_LIFETIME_FULFILLED':
      return {...state,
        total_life_time_bookings: {
          data: action.payload.data,
          errMsg: null
        }
      }

    case 'GET_TOTAL_BOOKINGS_LIFETIME_REJECTED':
      return {...state,
        total_life_time_bookings:{
          ...state.total_life_time_bookings,
          errMsg: handleResponseErr(action.payload)
        }
      }

    case 'GET_TOTAL_BOOKINGS_FULFILLED':
      return {...state,
        total_bookings: {
          data: action.payload.data,
          errMsg: null
        }
      }

    case 'GET_TOTAL_BOOKINGS_REJECTED':
      return {...state,
        total_bookings:{
          ...state.total_bookings,
          errMsg: handleResponseErr(action.payload)
        }
      }

    case 'GET_TOTAL_BOOKINGS_TODAY_FULFILLED':
      return {...state,
        today_bookings: {
          data: action.payload.data,
          errMsg: null
        }
      }
    case 'GET_TOTAL_BOOKINGS_TODAY_REJECTED':
      return {...state,
        today_bookings:{
          ...state.today_bookings,
          errMsg: handleResponseErr(action.payload)
        }
      }
  }

  return state

}
