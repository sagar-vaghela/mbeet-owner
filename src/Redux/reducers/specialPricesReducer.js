/*
* SpecialPrice Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  specialPrices: null,
  specialPrice: null,
  totalSpecialPrices: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function specialPricesReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_SPECIALPRICES_PENDING":
    case "GET_SPECIALPRICE_PENDING":
    case "ADD_SPECIALPRICE_PENDING":
    case "DELETE_SPECIALPRICE_PENDING":
    case "EDIT_SPECIALPRICE_PENDING":
    case "SEARCH_SPECIALPRICES_PENDING":
    case "FILTER_SPECIALPRICES_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, specialPrice: null}


    /*************************
    *   Index SpecialPrices
    **************************/
    /*
    * On Failed
    * Action: put error message on the state
    */
    case "GET_SPECIALPRICES_REJECTED":
    case "FILTER_SPECIALPRICES_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*
    * On Success
    * Action: put the specialPrices on the state
    */
    case "GET_SPECIALPRICES_FULFILLED":
    case "FILTER_SPECIALPRICES_FULFILLED":
      changes = {
        specialPrices: action.payload.data.data.all_periods,
        totalSpecialPrices: action.payload.data.data.totalPeriods,
        errMsg: null,
        fetching: false,
      }

      return {...state, ...changes}

    // On Delete
    case "DELETE_SPECIALPRICE_FULFILLED":
    case "RESTORE_SPECIALPRICE_FULFILLED":
      changes = {
        specialPrices : state.specialPrices.filter(specialPrice => specialPrice.id !== action.payload),
        fetching: false,
        errMsg: null,
        succMsg: null,
      }
      return {...state, ...changes}

    // On failed delete
    case "DELETE_SPECIALPRICE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Show SpecialPrice
    **************************/

    // On success get specialPrice
    case "GET_SPECIALPRICE_FULFILLED":
      changes = {
        specialPrice: action.payload.data.period,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On failed get specialPrice
    case "GET_SPECIALPRICE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Add SpecialPrice
    **************************/
    // On Success Add specialPrice
    case "ADD_SPECIALPRICE_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "ADD_SPECIALPRICE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Edit SpecialPrice
    **************************/
    // On Success
    case "EDIT_SPECIALPRICE_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "EDIT_SPECIALPRICE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Search SpecialPrices
    **************************/
    case "SEARCH_SPECIALPRICES_FULFILLED":
      changes = {
        specialPrices: action.payload.data.data.specialPrices,
        totalSpecialPrices: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

  } // / End Switch

  return state

}
