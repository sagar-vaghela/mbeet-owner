import React from 'react'
import FilterToolbar from './FilterToolbar'
import Pagination from './Pagination'
import RowUnit from '../Pages/Units/indexUnits/rowUnit/RowUnit'
import DummyRowUnit from '../Pages/Units/indexUnits/rowUnit/DummyRowUnit'
import Loading from './Loading'
import {FormattedMessage} from 'react-intl';

// function showing(total, current_page, display_count){
//
//   const start = (current_page === 1) ? current_page : ((current_page * display_count) - display_count)
//   const minus = (total - ((current_page - 1) * display_count))
//   const end = (minus > display_count) ? (current_page * display_count) : total
//   let result = ''
//   result += 'Showing '
//   result += (start > total) ? total : start
//   result += ' to '+end
//   result += ' of '
//   result += total+' entries'
//   return result
// }

function getResult(total, current_page, display_count){

  const start = (current_page === 1) ? current_page : ((current_page * display_count) - display_count)
  const minus = (total - ((current_page - 1) * display_count))
  const end = (minus > display_count) ? (current_page * display_count) : total
  let result = ''
  result = (start > total) ? total : start
  return result
}

function getEnd(total, current_page, display_count){

  const start = (current_page === 1) ? current_page : ((current_page * display_count) - display_count)
  const minus = (total - ((current_page - 1) * display_count))
  const end = (minus > display_count) ? (current_page * display_count) : total
  let result = ''
  result = end
  return result
}


const FlatsList = (props) => {
  const {units, loading, update, showList, errorMsg, rowShowing, onChangeShowing, onDelete, onRestore, onDisable, onEnable} = props
  return (
    <div>
      {!loading && <FilterToolbar
                        rowShowing={rowShowing}
                        onchange={onChangeShowing}
                        showList={showList}
                        onSearch={props.onSearch}
                        filters={props.filters}
                        onChangeFilter={props.onChangeFilter}
                        currentFilter={props.currentFilter}
                        search={props.search} />}

      {/* Units List content */}
      <div className="page-container units-container">
        {(loading || update) && <Loading error={errorMsg} />}
        {(loading || errorMsg) ? (
          <div className="card-deck">
            <DummyRowUnit />
            <DummyRowUnit />
            <DummyRowUnit />
            <DummyRowUnit />
          </div>
        ):(
          <div className="card-deck">

            {units.map(unit => {
              return (<RowUnit onDelete={onDelete} onRestore={onRestore} onDisable={onDisable} onEnable={onEnable} key={'unit'+unit.id} unit={unit} />)
            })}
          </div>
        )}

      </div>

      {!loading &&
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-6">
            <span className="showing-entries">
              <FormattedMessage id="showing" /> {getResult(props.total, props.page, props.rowShowing)} <FormattedMessage id="to" /> {getEnd(props.total, props.page, props.rowShowing)} <FormattedMessage id="of" /> {props.total} <FormattedMessage id="entries" />
            </span>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6">
            <Pagination
              onPageChange={props.onPageChange}
              total={props.total}
              show={props.rowShowing}
              currentPage={props.page} />
          </div>
        </div>
      }

    </div>
  )
}
export default FlatsList
