import React from 'react'
import './css/Breadcrumb.css'
import { Link } from 'react-router-dom'
import {FormattedMessage} from 'react-intl';

const Breadcrumb = (props) => {
  const paths = props.path
  const direction = localStorage.LOCALE == 'ar' && "breadcrumb-rtd"

  return (
  <ol className={"breadcrumb "+ direction}>
    <li className="breadcrumb-item"><Link to="/"><FormattedMessage id="home" /></Link></li>
    {paths.map((path, key) => {
      let active = ((key + 1) === paths.length) ? 'active' : false
      let pathLink = active ? path : <Link to={'/'+path}> <FormattedMessage id={path} /> </Link>
    return (
      <li key={'path'+key} className={'breadcrumb-item '+active}>
        {pathLink}
      </li>
    )
    })}
  </ol>
  )
}
export default Breadcrumb
