import React, {Component} from 'react'
import { NavLink }           from 'react-router-dom'

// Material UI
import People             from 'material-ui/svg-icons/social/people'
import LocationCity       from 'material-ui/svg-icons/social/location-city'
import DashboardIcon      from 'material-ui/svg-icons/action/dashboard'
import EventNoteIcon      from 'material-ui/svg-icons/notification/event-note'
import PlaceIcon          from 'material-ui/svg-icons/maps/place'
import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'
import FlatButton         from 'material-ui/FlatButton'

import {FormattedMessage} from 'react-intl';



class SidebarMenu extends Component{
  render(){
    const {path} = this.props
    const direction = localStorage.LOCALE == 'ar' && "sidebar-menu-rtd"
    return (
      <div className={"sidebar-menu " + direction}>

        <ul className="menu-list">
           <li>
             <NavLink to="/" exact activeClassName="active">
               <FlatButton
                 label={<FormattedMessage id="dashboard" defaultMessage="Dashboard"/>}
                 icon={<DashboardIcon />}
                 />
             </NavLink>
           </li>

           <li>
             <NavLink to="/units" activeClassName="active">
               <FlatButton
                 label={<FormattedMessage id="units" defaultMessage="Units"/>}
                 icon={<LocationCity />}
                 />
             </NavLink>
           </li>

           <li>
             <NavLink to="/bookings" activeClassName="active">
               <FlatButton
                 label={<FormattedMessage id="bookings" defaultMessage="Bookings" />}
                 icon={<EventNoteIcon />}
                 />
             </NavLink>
           </li>

        </ul>
      </div>
    )
  }
}


export default SidebarMenu
