import React from 'react'

import NavCollapse from './navCollapse/NavCollapse'
import UserProfileBtn from './userProfileBtn/UserProfileBtn'
import './navStyle.css'

const Navbar = (props) => {

  const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false
  const direction = localStorage.LOCALE === 'ar' && "en"
  return (
    <nav className={"navbar navbar-toggleable-md navbar-light bg-faded fixed-top " + direction}>
      <NavCollapse />
      {isLogged && <UserProfileBtn />}
    </nav>
  )
}

export default Navbar;
