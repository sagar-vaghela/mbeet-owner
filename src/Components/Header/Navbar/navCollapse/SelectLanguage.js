import React , {Component} from 'react'
import {connect} from 'react-redux'
import {setLocale} from '../../../../Redux/actions/setLocaleAction'

class SelectLanguage extends Component {

  constructor(props){
    super(props)
    this.handleLanguage = this.handleLanguage.bind(this)
  }

  handleLanguage(event)
  {
    const {dispatch} = this.props;
    dispatch(setLocale(event.target.value))
  }


  render() {
    const { LOCALE } = this.props
    return (
        <div className="topmenulist-LOCALE">
          <select value={LOCALE != undefined ? LOCALE : 'en'} onChange={this.handleLanguage}>
            <option value="en">English</option>
            <option value="ar">Arabic</option>
          </select>
        </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    LOCALE: store.setLocale.LOCALE
  }
}

const selectLanguage = connect(mapStateToProps)(SelectLanguage);

export default selectLanguage;
