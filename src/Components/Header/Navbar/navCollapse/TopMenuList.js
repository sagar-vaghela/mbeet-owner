import React , { Component } from 'react'
import { NavLink }           from 'react-router-dom'
import {FormattedMessage} from 'react-intl';
import './TopMenuList.css'
import {connect} from 'react-redux'

import {setLocale} from '../../../../Redux/actions/setLocaleAction'
import SelectLanguage from './SelectLanguage'


class TopMenuList extends Component {
  constructor(props){
    super(props)

    this.state = {comID: 'TopMenuList'}
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  render() {
    const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false
    return (
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          {!isLogged &&
            <NavLink to="/login" exact activeClassName="active">
              <FormattedMessage id="login" defaultMessage="Login" />
            </NavLink>
          }
          </li>
          <li className="nav-item">
            {!isLogged &&
            <NavLink to="/registration" exact activeClassName="active">
              <FormattedMessage id="registration" defaultMessage="Registration" />
            </NavLink>
          }
          </li>
          <li className="nav-item">
            <NavLink to="/about" exact activeClassName="active">
              <FormattedMessage id="about" defaultMessage="About" />
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/privacy" exact activeClassName="active">
              <FormattedMessage id="privacy" defaultMessage="Privacy" />
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink to="/contact_us" exact activeClassName="active">
              <FormattedMessage id="contact_us" defaultMessage="Contact Us" />
            </NavLink>
          </li>

          <li className="nav-item">
            <SelectLanguage />
          </li>

        </ul>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    LOCALE: store.setLocale.LOCALE
  }
}

const TopMenu = connect(mapStateToProps)(TopMenuList);

export default TopMenuList;
