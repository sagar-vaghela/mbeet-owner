import React from 'react'
import { Link }           from 'react-router-dom'

const Logo = () => {
  const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false
  return  (
      <div>
        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <Link to={isLogged ? "/" : "/home"} className="navbar-brand">
          <img alt="logo" src="./images/logo.png" className="img-responsive" />
        </Link>
      </div>
    )
}
export default Logo
