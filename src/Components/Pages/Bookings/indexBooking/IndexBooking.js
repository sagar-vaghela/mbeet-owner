import React, {Component} from 'react'
import {connect} from 'react-redux'
import moment from 'moment'

// Users Redux Actions
import {getBookings, cancelBooking } from '../../../../Redux/actions/bookingsActions'

import {deleteItem} from '../../../../Utilities/functions'

import RowBooking from './rowBookings/RowBookings'
import {DummyBookingRow} from './rowBookings/DummyBookingRow'
import DateFilter from './DateFilter'

import swal from 'sweetalert'
// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TableList} from '../../../GlobalComponents/GlobalComponents'

import EventNoteIcon      from 'material-ui/svg-icons/notification/event-note'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

import {FormattedMessage} from 'react-intl';

class IndexBooking extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      currentPage : 1,
      show: 10,
      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      filters: [
        {value: 0, params: 0, name: <FormattedMessage id="all" />},
        {value: 'true',params: 'booking_cancelled', name: <FormattedMessage id="cancelled" />},
        {value: '1', params: 'payment_status', name: <FormattedMessage id="pending" />},
        {value: '3', params: 'payment_status', name: <FormattedMessage id="confirmed" />},
        {value: '4', params: 'payment_status', name: <FormattedMessage id="completed" />},
      ],

      comID: 'bookings',
      currentFilter: 0,
      currentPage: 1,
      show: 10,
      sort_by: 'id',
      sort_direction: 'asc',
      search: '',
      dateFilters: {
        type: 1,
        from: null,
        to: null,
      }
    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleFilters = this.handleFilters.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleSorting = this.handleSorting.bind(this)
    this.handleShowing = this.handleShowing.bind(this)
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('show', this.getSession('show'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    const type = parseInt(this.getSession('type'))
    const from = this.getSession('from')
    const to = this.getSession('to')
    let dateFilters = null
    if(type && from && to){
      dateFilters = {
        type,
        from,
        to
      }
      this.setState({dateFilters})
    }


    const {filters} = this.state
    const offset = (this.getSession('currentPage') - 1) * this.getSession('show')
    // On Load the Component get the users
    const {dispatch} = this.props

    dispatch(getBookings(
      this.getSession('show'),
      offset,
      filters[this.getSession('currentFilter')],
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search'),
      dateFilters,
    )).then(() => {

      // stop loading if successfull fetched the users
      this.setState({loading: false})

    })
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const {dispatch} = this.props
    const {filters, dateFilters} = this.state
    const length = e.target.value.length
    const s = e.target.value

    if(length >= 3 || length === 0)
      dispatch(getBookings(
        this.getSession('show'),
        0,
        filters[this.getSession('currentFilter')],
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s,
        dateFilters,
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    const {filters, currentFilter, sort_direction, dateFilters} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })

    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getBookings(
      this.getSession('show'), // limit
      0, // offset
      filters[this.getSession('currentFilter')], // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
      dateFilters,
    ))

  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    const {filters, currentFilter, sort_by, sort_direction, search, dateFilters} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.show

    // get users
    dispatch(getBookings(
      this.state.show,  // limit
      offset, // offset
      filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
      dateFilters,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }


  /***************************
  * On change showing
  ****************************/
  handleShowing(count){
    const {dispatch} = this.props
    const {filters, currentFilter, sort_by, sort_direction, search, dateFilters} = this.state
    dispatch(getBookings(
      count, // limit
      0, // offset
      filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
      dateFilters,
    )).then(() => {
      this.setState({show: count, currentPage: 1})
      this.setSession('show', count)
      this.setSession('currentPage', 1)
    })
  }


  /***************************
  * On Change Filter
  ****************************/
  handleFilters(filter_type, value, index){
    const {dispatch} = this.props
    const {filters, dateFilters} = this.state
    this.setSession('currentFilter', index)
    this.setSession('currentPage', 1)
    dispatch(getBookings(
      this.state.show, // limit
      0, // offset
      filters[index],// filter
      this.state.sort_by, //sort by
      this.state.sort_direction, // sort direction
      this.state.search,
      dateFilters,
    ))
  }

  handleChangeDateFilter = (type, v) => {
    const {dispatch} = this.props
    const {
      dateFilters,
      show,
      filters,
      currentFilter,
      sort_by,
      sort_direction,
      search
    } = this.state

    const value = (type === 'type') ? v : moment(v).format('YYYY-MM-DD 12:00:00')
    dateFilters[type] = value
    this.setState({dateFilters})

    if(dateFilters.from && dateFilters.to){
      dispatch(getBookings(
        show, // limit
        0, // offset
        filters[currentFilter], // filter
        sort_by, // sort by
        sort_direction, // sort direction
        search,
        dateFilters
      ))
      .then(() => {
        this.setState({show: show, currentPage: 1})
        this.setSession('currentPage', 1)
        this.setSession('type', dateFilters.type)
        this.setSession('from', dateFilters.from)
        this.setSession('to', dateFilters.to)
      })
    }

    //alert(moment(value).format('YYYY-MM-DD 12:00:00'))
  }

  handleResetDateFilters = () => {
    const {dispatch} = this.props
    const {
      show,
      filters,
      currentFilter,
      sort_by,
      sort_direction,
      search,

    } = this.state



    dispatch(getBookings(
      show, // limit
      0, // offset
      filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
      null
    ))
    .then(() => {
      this.setState({show: show, currentPage: 1})
      this.setSession('currentPage', 1)

      this.setState({dateFilters:{
        type: 1,
        from: null,
        to: null,
      }})
      this.setSession('type', 1)
      this.setSession('from', '')
      this.setSession('to', '')
    })
  }

  render(){

    // Table columns
    const columns = [
      // {params: 'id', title: <FormattedMessage id="id" />},
      // {params: "user", title: <FormattedMessage id="user" />},
      // {params: "email" , title: <FormattedMessage id="email" />},
      // {params: "phone" , title: <FormattedMessage id="phone" />},
      // {params: "check_in", title: <FormattedMessage id="check-in" />},
      // {params: 'check_out', title: <FormattedMessage id="check-out" />},
      // {params: "created_at", title: <FormattedMessage id="created-date" />},
      // {params: "total_amount", title: <FormattedMessage id="total_amount" />},
      {params: 0, title: <FormattedMessage id="id" />},
      {params: 0, title: <FormattedMessage id="user" />},
      {params: 0 , title: <FormattedMessage id="email" />},
      {params: 0 , title: <FormattedMessage id="phone" />},
      {params: 0, title: <FormattedMessage id="check-in" />},
      {params: 0, title: <FormattedMessage id="check-out" />},
      {params: 0, title: <FormattedMessage id="created-date" />},
      {params: 0, title: <FormattedMessage id="total_amount" />},
    ]

    // Store props
    const {bookings, total, errMsg, fetching} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: (<FormattedMessage id="bookings_list" />),
      icon: (<EventNoteIcon className="pagetitle-icon"/>),
    }

    // TableList Options
    const tableListOptions = {
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: total,
      rowShowing: parseInt(this.getSession('show')),
      onChangeShowing: this.handleShowing,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      // onSearch: this.handleSearch,
      // search: this.getSession('search'),
      onError: errMsg,
      // filters: this.state.filters,
      onChangeFilter: this.handleFilters,
      currentFilter: this.getSession('currentFilter'),
      onSorting: this.handleSorting,
      // customFilter: (<DateFilter
      //                   onChange={this.handleChangeDateFilter}
      //                   dateFilters={this.state.dateFilters}
      //                   reset={this.handleResetDateFilters}
      //                 />)
    }

    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={[<FormattedMessage id="bookings" />]} />
          <PageHeader {...pageHeaderOptions} />

          <TableList {...tableListOptions}>

            { // if fetched users
              !this.state.loading ?
              (
                bookings.map(booking => {
                  return (<RowBooking key={'booking'+booking.id} booking={booking} onCancel={this.handleCancelBooking} />)
                })
              )
              :
              (
                DummyBookingRow()
              )
            /* end if */ }

          </TableList>

        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    bookings: store.bookings.bookings,
    total: store.bookings.total,
    errMsg: store.bookings.errMsg,
    fetching: store.bookings.fetching,
  }
}

IndexBooking = connect(mapStateToProps)(IndexBooking)
export default IndexBooking
