import React, {Component} from 'react'
import DatePicker from 'material-ui/DatePicker';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import {FormattedMessage} from 'react-intl';



const DateFilter = ({onChange, dateFilters, reset}) => {

  const toOptions = dateFilters.to ? {value: new Date(dateFilters.to)} : {value: {}}
  const fromOptions = dateFilters.from ? {value: new Date(dateFilters.from)} : {value: {}}


  return (
      <div className="date-filter">
        <div><FormattedMessage id="date" /> <FormattedMessage id="filter" />: </div>
        <div className="select-list">
          <DropDownMenu value={dateFilters.type} onChange={(e, o, v) => onChange('type', v)} style={{width: '200px'}}>
            <MenuItem value={1} primaryText={<FormattedMessage id="created-date" />} />
            <MenuItem value={2} primaryText={<FormattedMessage id="check-in" />} />
          </DropDownMenu>
        </div>
        <DatePicker hintText={<FormattedMessage id="from" />} container="inline" {...fromOptions} onChange={(n, date) => onChange('from', date)} />
        <DatePicker hintText={<FormattedMessage id="To" />} container="inline" {...toOptions} onChange={(n, date) => onChange('to', date)} />
        <FlatButton label={<FormattedMessage id="reset" />} onClick={reset} disabled={(dateFilters.from && dateFilters.to) ? false : true} />
      </div>
    )
    }
export default DateFilter
