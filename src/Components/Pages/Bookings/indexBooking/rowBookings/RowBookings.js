import React from 'react'
import { Link }           from 'react-router-dom'

import {paymentsStatus} from '../../../../../Utilities/options'

import dateFormat  from 'dateformat'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';


const RowBooking = (props) => {
  const {booking, onCancel} = props
  return (
    <tr>
      <td>{booking.id}</td>
      <td>{booking.user.name}</td>
      <td>{booking.user.email}</td>
      <td>{booking.user.phone ? booking.user.phone : ''}</td>
      <td>{dateFormat(new Date(booking.check_in),'ddd, d mmm yyyy h:MM TT')}</td>
      <td>{dateFormat(new Date(booking.check_out),'ddd, d mmm yyyy h:MM TT')}</td>
      <td>{dateFormat(new Date(booking.created_at),'ddd, d mmm yyyy h:MM TT')}</td>
      <td>{booking.total_amount}</td>
    </tr>
  )
}

export default RowBooking
