import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link }           from 'react-router-dom'
import {FormattedMessage} from 'react-intl'

// Material UI
import RaisedButton         from 'material-ui/RaisedButton'
import FlatButton           from 'material-ui/FlatButton'

import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator'

class ResetPasswordForm extends Component{
  constructor(props) {
    super(props)

    this.state = {
      form : {}
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
  }

  render(){
    const {onSubmit} = this.props
    const { form } = this.state

    return(
      <ValidatorForm
                  ref="form"
                  onSubmit={onSubmit}
                  >
        {!this.props.done &&
        <TextValidator
            floatingLabelText={<FormattedMessage id="email" />}
            onChange={this.handleChange}
            name="email"
            value={form.email}
            style={{width: '100%'}}
            validators={['required', 'isEmail']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />,<FormattedMessage id="email_is_not_valid" />]}
          /> }

        <div className="actions">
          {!this.props.done &&
          <RaisedButton type="submit" label={<FormattedMessage id="send" /> }primary={true}  className="submit-btn" />
        }
          <Link to="/login">
            <FlatButton label={<FormattedMessage id="login" />} />
          </Link>
        </div>
      </ValidatorForm>
    )
  }
}


// proptypes onSubmit
ResetPasswordForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}
export default ResetPasswordForm
