import React from 'react'
import {FormattedMessage} from 'react-intl';


const TotalBookings = ({title, pending, cancelled, confirmed, completed}) => (
  <div className="dashboard-widget">
    <div className="middle-box">
      <span className="title"><FormattedMessage id={title} /></span>
      <span className="big-number">
        {pending+cancelled+confirmed+completed}
      </span>
    </div>

    <div className="row space-top">
      <div className="col-lg-3 col-md-3 center">
        <span className="title-md"><FormattedMessage id="pending" /></span>
        <span className="number-md yellow">{pending}</span>
      </div>

      <div className="col-lg-3 col-md-3 center">
        <span className="title-md"><FormattedMessage id="cancelled" /></span>
        <span className="number-md red">{cancelled}</span>
      </div>

      <div className="col-lg-3 col-md-3 center">
        <span className="title-md"><FormattedMessage id="confirmed" /></span>
        <span className="number-md yellow">{confirmed}</span>
      </div>

      <div className="col-lg-3 col-md-3 center">
        <span className="title-md"><FormattedMessage id="completed" /></span>
        <span className="number-md red">{completed}</span>
      </div>
    </div>
  </div>
)
export default TotalBookings
