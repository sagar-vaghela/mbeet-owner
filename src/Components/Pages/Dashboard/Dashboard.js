import React, {Component} from 'react'
import {connect} from 'react-redux'
import {
  getRegisteredUsers,
  getTotalBookings
} from '../../../Redux/actions/dashboardActions'
import moment from 'moment'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading} from '../../GlobalComponents/GlobalComponents'

import People from 'material-ui/svg-icons/social/people'

import TotalBookings from './TotalBookings'

import './dashboard.css'
// Components
import { CSSTransitionGroup } from 'react-transition-group'

class Dashboard extends Component{

  componentWillMount(){
    const {dispatch} = this.props

    // total bookings
    dispatch(getTotalBookings())
    dispatch(getTotalBookings(moment().format('YYYY-MM-DD'), 1))
    dispatch(getTotalBookings(null,null,true))

  }

  render(){
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Dashboard',
      icon: (<People className="pagetitle-icon"/>),
    }

    const {total_life_time_bookings , total_bookings, today_bookings} = this.props

    return (
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <div className="dashboard-container">
            <div className="row">
              <div className="col-lg-6 col-md-6">
                <div className="widget-container">
                  {(!total_life_time_bookings.data || total_life_time_bookings.errMsg) &&
                    <div className="loading-container">
                      <Loading error={total_life_time_bookings.errMsg} />
                    </div>
                  }

                  {total_life_time_bookings.data &&
                    <TotalBookings
                      title="total_life_time_bookings"
                      pending={total_life_time_bookings.data.pending}
                      cancelled={total_life_time_bookings.data.cancelled}
                      confirmed={total_life_time_bookings.data.confirmed}
                      completed={total_life_time_bookings.data.completed}
                      />
                  }
                </div>
              </div>

              <div className="col-lg-6 col-md-6">
                <div className="widget-container">
                  {(!total_bookings.data || total_bookings.errMsg) &&
                    <div className="loading-container">
                      <Loading error={total_bookings.errMsg} />
                    </div>
                  }

                  {total_bookings.data &&
                    <TotalBookings
                      title="this_month_bookings"
                      pending={total_bookings.data.pending}
                      cancelled={total_bookings.data.cancelled}
                      confirmed={total_bookings.data.confirmed}
                      completed={total_bookings.data.completed}
                    />
                  }
                </div>
              </div>
            </div>

            <div className="row space-top-row">
              <div className="col-lg-6 col-md-6 ">
                <div className="widget-container">
                  {(!today_bookings.data || today_bookings.errMsg) &&
                    <div className="loading-container">
                      <Loading error={today_bookings.errMsg} />
                    </div>
                  }

                  {today_bookings.data &&
                    <TotalBookings
                      title="today_bookings"
                      pending={today_bookings.data.pending}
                      cancelled={today_bookings.data.cancelled}
                      confirmed={today_bookings.data.confirmed}
                      completed={today_bookings.data.completed}
                      />
                  }
                </div>
              </div>

              <div className="col-lg-6 col-md-6">
                <div className="widget-container">
                </div>
              </div>
            </div>
          </div>
        </CSSTransitionGroup>
      </PagesContainer>

    )
  }
}

const mapStateToProps = (state) => {
  console.log("aa mapStateToProps ma che");
  console.log(state);
  return {
    total_bookings: state.dashboard.total_bookings,
    today_bookings: state.dashboard.today_bookings,
    total_life_time_bookings: state.dashboard.total_life_time_bookings,
  }
}
Dashboard = connect(mapStateToProps)(Dashboard)
export default Dashboard
