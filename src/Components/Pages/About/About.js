import React from 'react'
import {FormattedMessage} from 'react-intl';

const About = () => (
  <div>
    <div className="container">
      <div className="single-page-content">
        <div className="page-container">
          <h1 className="page-title"><FormattedMessage id="about" /></h1>
          <p><FormattedMessage id="about_p" /></p>
        </div>
      </div>
    </div>
  </div>
)
export default About
