/*
* ContactUs - Component connected to redux
*/

// Import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'
import {FormattedMessage} from 'react-intl';

// ContactUs Action - Redux Action
import {contactUs , initAuth} from '../../../Redux/actions/authActions'

// Components
// import ContactUsContainer from "../registrationContainer/ContactUsContainer"
import ContactUsForm from './ContactUsForm'

// Global Components
import Loading from '../../GlobalComponents/Loading'
import AlertMsg from '../../GlobalComponents/AlertMsg'

// External Packages
import serializeForm from 'form-serialize'

class ContactUs extends Component{

  constructor(props){
    super(props)
    this.handleContactUs = this.handleContactUs.bind(this)
  }

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(initAuth())
  }

  // On ContactUs form submit
  handleContactUs(e){
    e.preventDefault()

    // get dispatch from props
    const {dispatch} = this.props

    // get form data
    let formData = serializeForm(e.target, { hash: true })


    // dispatch
    dispatch(contactUs(formData))
  }


  render(){
    // get messages from Redux store
    const {errMsg, succMsg, fetching} = this.props
    // const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false

    return(
        <div>
          <div className="container">
            <div className="single-page-content">

              <div className="page-container">
                <h1 className="page-title"><FormattedMessage id="contact_us" /></h1>

                    <div className="contact-Info ">
                        {succMsg && <Redirect to="/home" />}

                        {/* Loading */}

                          {fetching && <Loading />}

                          {/* after dispatch, display the error or success message */}
                          {(errMsg || succMsg) && <AlertMsg error={errMsg} success={succMsg} />}

                          {/* registration form */}
                          <ContactUsForm onSubmit={this.handleContactUs}  />
                    </div>

                    <div className="contact-Details">
                      <h2><FormattedMessage id="contact_information" /></h2>
                      <hr/>
                      <p><i className="fa fa-envelope fa-lg"></i> :<a href="#"> info@mbeetapp.com</a></p>
                      <p><i className="fa fa-phone fa-lg"></i> : (+966) 920007691</p>
                    </div>

              </div>
            </div>
          </div>
        </div>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    errMsg: store.auth.errMsg,
    succMsg: store.auth.succMsg,
    fetching: store.auth.fetching,
  }
}

const ContactUsApp = connect(mapStateToProps)(ContactUs);

export default ContactUsApp
