import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link }           from 'react-router-dom'
import {FormattedMessage} from 'react-intl';

// Material UI
import RaisedButton         from 'material-ui/RaisedButton'
import FlatButton           from 'material-ui/FlatButton'

import { ValidatorForm, TextValidator, SelectValidator, DateValidator} from 'react-material-ui-form-validator'

import MenuItem from 'material-ui/MenuItem';

import {countryCode} from '../../../Utilities/countries_code'

class ContactUsForm extends Component{

  constructor(props) {
    super(props)

    this.state = {
      form : {},
      country: '',
    }
    this.handleCountryCode = this.handleCountryCode.bind(this)
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
  }

  handleCountryCode = (event, index, value) => this.setState({country: value})

  render(){
    const {onSubmit , succMsg} = this.props;
    const { form } = this.state;

    return(
      <ValidatorForm
                  ref="form"
                  onSubmit={onSubmit}
                  >

      <div className="form-field">
        <TextValidator
            floatingLabelText={<FormattedMessage id="name" />}
            onChange={this.handleChange}
            name="name"
            value={form.name}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />
      </div>

      <div className="form-field">
        <TextValidator
            floatingLabelText={<FormattedMessage id="email" />}
            onChange={this.handleChange}
            name="email"
            value={form.email}
            style={{width: '100%'}}
            validators={['required', 'isEmail']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />,<FormattedMessage id="email_is_not_valid" />]}
          />
      </div>


      <div className="form-field">

        <SelectValidator
           floatingLabelText={<FormattedMessage id="select_country" />}
           value={this.state.country}
           onChange={this.handleCountryCode}
           name="country"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={[<FormattedMessage id="this_field_is_required" />]}
         >
          {countryCode.map(country => {
            return (<MenuItem
                      key={country.name}
                      value={country.name}
                      primaryText={country.name} />)
          })}

         </SelectValidator>
         <input  name="country" value={this.state.country} type="text" style={{display: 'none'}} />

      </div>

      <div className="form-field">
          <TextValidator
            floatingLabelText={<FormattedMessage id="phone" />}
            onChange={this.handleChange}
            name="phone"
            type="number"
            value={form.phone}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
           />
      </div>

      <div className="form-field">
        <TextValidator
            floatingLabelText={<FormattedMessage id="message" />}
            onChange={this.handleChange}
            name="message"
            value={form.message}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />
      </div>

      <div className="form-field submit">
        <div className="actions">
          <RaisedButton type="submit" label={<FormattedMessage id="send" />} primary={true}  className="submit-btn" />
        </div>
      </div>
      </ValidatorForm>
    )
  }
}

// proptypes onSubmit
ContactUsForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}
export default ContactUsForm
