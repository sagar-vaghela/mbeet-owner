import React, { Component } from 'react'; // React & React component
import {connect} from 'react-redux'

import {FormattedMessage} from 'react-intl';
import { Player } from 'video-react';

import {setLocale} from '../../../Redux/actions/setLocaleAction'


class Home extends Component {

  componentWillMount(){
    const {dispatch} = this.props;
    if (localStorage.LOCALE === "undefined")
    {
      dispatch(setLocale('en'))
    }
    else {
      dispatch(setLocale(localStorage.LOCALE))
    }
  }

  render() {
    const {LOCALE} = this.props;
    return (
      <div>
        <div className="container">
          <div className="single-page-content">

            <div className="page-container">
              <h1 className="page-title"><FormattedMessage id="home" /></h1>
              <Player
                playsInline
                poster="/assets/poster.png"
                src={LOCALE === 'en' ? "videos/eng.mp4" : "videos/arb.mp4"}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    LOCALE: store.setLocale.LOCALE
  }
}

const home = connect(mapStateToProps)(Home);

export default home;
