/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'


// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';

import SimpleMap from '../../addUnit/addUnitSteps/maps'

import { TextValidator, SelectValidator, AutoCompleteValidator} from 'react-material-ui-form-validator'

import AutoComplete from 'material-ui/AutoComplete';

class Step1 extends Component{

  constructor(props){
    super(props)
    this.state = {
      users: 0,
      user_id: props.unit.owner.user_id,
      searchText: props.unit.owner.name,
      lat: parseInt(props.unit.latitude),
      lng: parseInt(props.unit.longitude),
    }

    this.handleChangeUserID = this.handleChangeUserID.bind(this)
  }

  handleChangeUserID(user){
    this.setState({user_id: user.id, searchText: user.name});
  }

  componentWillMount(){
    const {unit} = this.props
    this.setState({
      users: unit.owner.user_id
    })
  }

  handleSelectUsers     = (event, index, value) => this.setState({users: value});

  render(){

    const {users, display, unit} = this.props

    return(
      <div style={{display: (display ? 'block' : 'none')}}>



        {/* Select User */}
        {users &&
          <div className="form-field">

          <input  name="user_id" value={this.state.user_id} type="text" className="hidden-input" />
          <AutoCompleteValidator
            floatingLabelText="Owner"
            filter={AutoComplete.caseInsensitiveFilter}
            dataSource={users}
            dataSourceConfig={ {text: 'name', value: 'id'}  }
            searchText={this.state.searchText}
            value={this.state.user_id}
            openOnFocus={true}
            fullWidth={true}
            onNewRequest={this.handleChangeUserID}
            name="uid"
            validators={['required']}
            errorMessages={['this field is required']}
          />


          </div> }

        {/* Title English */}
        <div className="form-field">
          <TextField
           hintText="Title English"
           style={{width: '100%'}}
           name="title_en"
           defaultValue={unit.title_en}
         />
       </div>

         {/* Title Arabic */}
         <div className="form-field">
             <TextField
              hintText="Title Arabic"
              style={{width: '100%'}}
              name="title_ar"
              defaultValue={unit.title_ar}
            />
          </div>

        {/* Description English*/}
        <div className="form-field">
          <TextField
           hintText="Description English"
           style={{width: '100%'}}
           name="body_en"
           multiLine={true}
           defaultValue={unit.body_en}
         />
       </div>

        {/* Description Arabic */}
        <div className="form-field">
           <TextField
            hintText="Description Arabic"
            style={{width: '100%'}}
            name="body_ar"
            defaultValue={unit.body_ar}
            multiLine={true}
          />
        </div>

      </div>
    )
  }
}
export default Step1
