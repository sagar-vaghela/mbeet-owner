/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'


class Step2 extends Component{
  nsub = this.props.unit.number_of_subunits
  constructor(props){
    super(props)
    const {number_of_subunits} = props.unit
    this.state = {
      unit_type: props.unit.unit_type,
      unit_class: props.unit.unit_class.toUpperCase(),
      unit_status: props.unit.unit_status,
      unit_enabled: props.unit.unit_enabled,

      form: {
        sub_unit: false
      },
    }

    this.hanldeUnitStatus =  this.hanldeUnitStatus.bind(this)
  }

  handleSelectUnitType  = (event, index, value) => this.setState({unit_type: value})
  handleSelectUnitClass  = (event, index, value) => this.setState({unit_class: value})
  hanldeUnitStatus = (event, isInputChecked) => this.setState({unit_status: isInputChecked})


  handleChangeField = (event) => {
    const { form } = this.state;
    form[event.target.id] = event.target.value
    this.setState({ form })
  }

  componentWillMount(){
    const {form} = this.state
    let images = []
    this.props.unit.images.map(image => {
      images.push(image.url)

    })
    this.setState({images: images})

  }

  subunitsShow = () => {
    const {form} = this.state
    if(!form.sub_unit) return this.nsub - 1

    return form.sub_unit
  }

  subunitsSend = () => {
    const {form}    = this.state

    if(!form.sub_unit) return (this.nsub === 1) ? 0 : this.nsub

    if(parseInt(form.sub_unit) === 0) return 0

    return parseInt(form.sub_unit) + 1

  }

  render(){
    const {display, unit} = this.props
    const {form} = this.state

    return(
      <div style={{display: (display ? 'block' : 'none')}}>
        <div className="form-field">
          <Toggle
            label="Published"
            defaultToggled={this.state.unit_status}
            onToggle={this.hanldeUnitStatus}
            />
            <input type="hidden" name="unit_status" value={this.state.unit_status} />
        </div>

        <div className="form-field">
          <Toggle
            label="User Enabled"
            defaultToggled={this.state.unit_enabled}
            onToggle={this.hanldeUnitEnabled}
            />
            <input type="hidden" name="unit_enabled" value={this.state.unit_enabled} />
        </div>

      {/* Price* */}
      <div className="form-field">
          <TextField
           floatingLabelText="Price"
           style={{width: '100%'}}
           name="price"
           type="number"
           defaultValue={unit.price}
         />
      </div>

      {/* Service Charge* */}
      <div className="form-field">
          <TextField
           floatingLabelText="Service Charge"
           style={{width: '100%'}}
           name="service_charge"
           defaultValue={unit.service_charge}
           type="number"
         />
      </div>

      {/* Unit type */}
      <div className="form-field">
        <SelectField
           floatingLabelText="Unit Type"
           value={this.state.unit_type}
           onChange={this.handleSelectUnitType}
           name="unit_type"
           style={{width: '100%'}}
         >
           <MenuItem value={1} primaryText="Resort" />
           <MenuItem value={2} primaryText="Villa" />
           <MenuItem value={3} primaryText="Apartment" />
           <MenuItem value={4} primaryText="Shaleeh" />
           <MenuItem value={5} primaryText="Private Room" />
           <MenuItem value={6} primaryText="Shared Room" />
         </SelectField>
         <input type="hidden" name="unit_type" value={this.state.unit_type} />
      </div>

      {/* Unit Class */}
      <div className="form-field">
        <SelectField
           floatingLabelText="Unit Class"
           value={this.state.unit_class}
           onChange={this.handleSelectUnitClass}
           name="unit_class"
           style={{width: '100%'}}
         >
           <MenuItem value="A" primaryText="A" />
           <MenuItem value="B" primaryText="B" />
           <MenuItem value="C" primaryText="C" />
         </SelectField>
         <input type="hidden" name="unit_class" value={this.state.unit_class} />
      </div>

      {/* Total Area */}
      <div className="form-field">
         <TextField
          floatingLabelText="Total Area"
          style={{width: '100%'}}
          name="total_area"
          type="number"
          defaultValue={unit.total_area}
        />
      </div>


      <div className="form-field">
        <TextValidator
            floatingLabelText="Number of Subunits"
            onChange={this.handleChangeField}
            name="sub_unit_fake"
            id="sub_unit"
            type="number"
            min="0"
            value={this.subunitsShow()}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />
          <input type="hidden" name="number_of_subunits" value={this.subunitsSend()}
          />
      </div>


      </div>
    )
  }

}

export default Step2
