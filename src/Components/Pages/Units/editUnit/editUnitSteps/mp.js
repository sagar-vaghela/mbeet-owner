
import React, {Component} from 'react'
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps"

class GettingStartedGoogleMap extends Component{
  render(){
    return(
      <GoogleMap
    ref={this.props.onMapLoad}
    defaultZoom={3}
    defaultCenter={{ lat: -25.363882, lng: 131.044922 }}
    onClick={this.props.onMapClick}
  >
    {this.props.markers.map(marker => (
      <Marker
        {...marker}
        onRightClick={() => this.props.onMarkerRightClick(marker)}
      />
    ))}
  </GoogleMap>
    )
  }
}

export default GettingStartedGoogleMap;
