import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

import People             from 'material-ui/svg-icons/social/people'

const AnyReactComponent = ({ text }) => (
  <People style={{
    position: 'relative', color: 'red',
    height: 40, width: 60, top: -20, left: -30,
  }} />

);



class SimpleMap extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      lat: '59.955413',
      lng: '30.337844'
    }
    this.handleClick = this.handleClick.bind(this)
  }
  static defaultProps = {
    center: {lat: 59.95, lng: 30.33},
    zoom: 11
  };

  handleClick(e){
    this.setState({lat: e.lat, lng: e.lng})

  }
  render() {
    return (
       <GoogleMapReact
        defaultCenter={this.props.center}
        defaultZoom={this.props.zoom}
        onClick={this.handleClick}
      >
        <AnyReactComponent
          lat={this.state.lat}
          lng={this.state.lng}
        />
      </GoogleMapReact>
    );
  }
}

export default SimpleMap
