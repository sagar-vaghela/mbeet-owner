/*
* AddUnit: Compnent
* Child of: Units Container
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import {addUnit, clearMsg} from '../../../../Redux/actions/unitsActions'
import {cloudinaryUpload, imagePath} from '../../../../Utilities/cloudinaryUpload'
import {getUsers} from '../../../../Redux/actions/usersActions'
import {getCities} from '../../../../Redux/actions/citiesActions'
import {getRules, getAmenities} from '../../../../Redux/actions/unitsActions'

// Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  FilterToolbar,
  FlatsList,
  Loading,
  AlertMsg,} from '../../../GlobalComponents/GlobalComponents'

import Form from '../Form/Form'


// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

import {FormattedMessage} from 'react-intl';

class AddUnit extends Component{

  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      images: [],
      loading: false,
    }

    this.handleImages = this.handleImages.bind(this)
  }

  handleImages(images){
    this.setState({images})
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch} = this.props
    const {images} = this.state
    var formData = new FormData(e.target)

    if(images.length > 0){
      this.setState({loading: true})
      cloudinaryUpload(images, false).then(response => {
        response.map(img => {

          formData.append('pictures_attributes[][name]', imagePath(img.url));
        })
        dispatch(addUnit(formData))
        this.setState({loading: false})
      })

    }else{
      dispatch(addUnit(formData))
    }
  }


  componentWillMount(){
    const {dispatch, match} = this.props

    dispatch(getUsers(100000, 0, {params: 'role', value:'owner'}))
    dispatch(getCities(10000))
    dispatch(getRules())
    dispatch(getAmenities())
  }


  // render
  render(){

    // Store props
    const {fetching, errMsg, succMsg, users, cities, rules, amenities} = this.props
    const {loading} = this.state

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: (<FormattedMessage id="add_new_unit" />),
      icon: (<LocationCity className="pagetitle-icon"/>),
    }


    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units',<FormattedMessage id="add_new_unit" />]} />
          <PageHeader {...pageHeaderOptions} />
          <div className="page-container">

            {(
                fetching
                || (!users || !cities || !rules || !amenities)
                || succMsg
                || loading)
            && <Loading success={succMsg} />}

            {(users && cities && rules && amenities) &&
              <Form
                formType="add"
                handleSubmit={this.handleSubmit}
                handleImages={this.handleImages}
                images={this.state.images}
                formType="add"
                users={users}
                cities={cities}
                rules={rules}
                amenities={amenities}
                 />
              }

            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}


const mapStateToProps = (store) => {
  return {
    errMsg: store.units.errMsg,
    succMsg: store.units.succMsg,
    fetching: store.units.fetching,
    users: store.users.users,
    cities: store.cities.cities,
    rules: store.units.rules,
    amenities: store.units.amenities,
  }
}

AddUnit = connect(mapStateToProps)(AddUnit)
export default AddUnit
