/*
* AddSpecialPriceForm: Object
* Child of: AddSpecialPrice
*/

// Main Packages
import React, {Component} from 'react'

// External Packages

import {Loading} from '../../../../GlobalComponents/GlobalComponents'

// Material UI
import RaisedButton from 'material-ui/RaisedButton';

import { ValidatorForm, TextValidator, DateValidator , SelectValidator } from 'react-material-ui-form-validator'

// External Packages
import $ from 'jquery'

import MenuItem from 'material-ui/MenuItem';

import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'
import Checkbox from 'material-ui/Checkbox';

// Style
import './addSpecialPrice.css'

import {FormattedMessage} from 'react-intl';


class AddSpecialPriceForm extends Component {
  constructor(props){
    super(props)

    const years = []
    for (let i = 2018; i < 2050; i++) {
        years.push({year: i});
    }

    this.state = {
      loading: false,
      for_the_days: [{ id : 0 , name : "sunday" , checked : false },{ id:1 , name: "monday" , checked : false },{ id : 2 , name : "tuesday" , checked : false },{ id : 3, name : "wednesday" , checked : false },{ id : 4 , name : "thursday" , checked : false },{ id : 5 ,name: "friday" , checked : false },{ id : 6 ,name: "saturday" , checked : false }],
      selected_days: [],
      years: years,
      selected_year: '',
      form: {},
    }

    this.handleYears = this.handleYears.bind(this)
    this.handlefor_the_days = this.handlefor_the_days.bind(this)
    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleYears = (event, index, value) => this.setState({selected_year: value})

  handlefor_the_days(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_days : this.state.selected_days.filter(an => an != parseInt(e.target.value))})
      obj.parent().removeClass('active')
      this.state.for_the_days.filter(ftd => ftd.id == parseInt(e.target.value) ? ftd.checked = false : '')
    }else{
      this.setState({selected_days : [...this.state.selected_days, parseInt(e.target.value)]})
      obj.parent().addClass('active')
      this.state.for_the_days.filter(ftd => ftd.id == parseInt(e.target.value) ? ftd.checked = true : '')
    }
  }

  handleChangeField(event){
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
  }

  // handleOnChangeFromPeriod_date = (a, data) => this.setState({period_date: data})

  handleOnChangeFromStarting_date = (a, data) => this.setState({starting_date: data})
  handleOnChangeFromEnd_date = (a, data) => this.setState({end_date: data})


  render(){
    const { starting_date , end_date , form , for_the_days , years } = this.state;

    return(
      <ValidatorForm ref="form" onSubmit={this.props.handleSubmit}>

        {this.state.loading && <Loading />}

        <input type="hidden" name="unit_id" value={this.props.unit_id} />
          {years &&
            <div className="form-field">

              <SelectValidator
                 floatingLabelText={<FormattedMessage id="select_year" />}
                 value={this.state.selected_year}
                 onChange={this.handleYears}
                 name="selected_year"
                 style={{width: '100%'}}
               >
                {years.map(year => {
                  return (<MenuItem
                            key={year.year}
                            value={year.year}
                            primaryText={year.year} />)
                })}

               </SelectValidator>
               <input  name="selected_year" value={this.state.selected_year} type="text" style={{display: 'none'}} />
            </div>
          }


          {for_the_days &&
          <div className="form-field">
            <div>
              <label><FormattedMessage id="for_the_day" /></label>
            </div>
            <div className="row">
              {for_the_days.map(for_the_day => {
                return (
                  <div key={for_the_day.id} className="for-the-day-item">
                    <Checkbox
                      checked={for_the_day.checked}
                      label={<FormattedMessage id={for_the_day.name} />}
                      className="for-the-day-item"
                      value={for_the_day.id}
                      onClick={this.handlefor_the_days}
                      name="for-the-day"
                    />
                  </div>
                )
              })}
              {this.state.selected_days.map((selected_day, key) => {
                return (<input key={selected_day} type="hidden" name="selected_days[]" value={selected_day} />)
              })}
            </div>

          </div> }

          <div className="form-field">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-sm-6">
                <DateValidator
                  floatingLabelText={<FormattedMessage id="from" />}
                  name="from"
                  mode="landscape"
                  minDate={new Date()}
                  value={starting_date ? starting_date : null}
                  onChange={this.handleOnChangeFromStarting_date}
                  mode="landscape"
                  container="inline"
                  validators={this.state.selected_year ? '' : ['required']}
                  errorMessages={[<FormattedMessage id="this_field_is_required" />]}
                />
                <input type="hidden" name="starting_date" value={starting_date} />
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6">
                <DateValidator
                  floatingLabelText={<FormattedMessage id="to" />}
                  container="inline"
                  name="to"
                  disabled={!starting_date}
                  minDate={starting_date}
                  mode="landscape"
                  value={end_date ? end_date : null}
                  onChange={this.handleOnChangeFromEnd_date}
                />
                <input type="hidden" name="end_date" value={end_date} />
              </div>
            </div>

          </div>

        <div className="form-field">
          <TextValidator
              floatingLabelText={<FormattedMessage id="price" />}
              onChange={this.handleChangeField}
              name="price"
              value={form.price? form.price : ''}
              style={{width: '100%'}}
              validators={['required']}
              errorMessages={[<FormattedMessage id="this_field_is_required" />]}
            />
        </div>

      <div className="form-field submit">
        <RaisedButton label={<FormattedMessage id="create_special_prices" />} type="submit" primary={true} />
      </div>

      </ValidatorForm>
    )
  }

}
export default AddSpecialPriceForm;
