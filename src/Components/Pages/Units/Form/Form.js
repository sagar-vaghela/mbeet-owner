/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import {getUsers} from '../../../../Redux/actions/usersActions'
import {getCities} from '../../../../Redux/actions/citiesActions'
import {getRules, getAmenities} from '../../../../Redux/actions/unitsActions'
import { ValidatorForm} from 'react-material-ui-form-validator'

// Components
import Step1 from './addUnitSteps/Step1'
import Step2 from './addUnitSteps/Step2'
import Step3 from './addUnitSteps/Step3'
import Step4 from './addUnitSteps/Step4'
import Step5 from './addUnitSteps/Step5'
import Step6 from './addUnitSteps/Step6'

// External Packages
//import serializeForm from 'form-serialize'

// Style
import './addUnitStyle.css'

// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

import {
  Step,
  Stepper,
  StepButton,
} from 'material-ui/Stepper';

import {FormattedMessage} from 'react-intl';


class AddUnitForm extends Component {


  fields:array = [
    {step: 1, fields: ['user_id', 'title_en', 'title_ar', 'body_en', 'body_ar']},
    {step: 2, fields: ['price', 'service_charge', 'unit_type', 'unit_class', 'total_area']},
    {step: 3, fields: ['city']},
    {step: 4, fields: ['available_as', 'number_of_guests', 'number_of_rooms', 'number_of_beds', 'number_of_baths']},
    {step: 5, fields: ['rules']},
  ]

  constructor(props){
    super(props)

    this.state = {
      users: null,
      room_type: null,
      unit_type: null,
      available_as: null,
      city: null,
      rules: [],
      amenities: [],
      stepIndex: 0,
      displayMessage: false,
      form: {}
    }

    this.handelFormErrors = this.handelFormErrors.bind(this)
  }

  handleFildsValues = (id, value) => {
    const {form} = this.state
    form[id] = value
    this.setState({form})

    if (this.props.formType === 'edit')
    {
      if (
         form.user_id !== this.props.unit.owner.user_id
        || form.title_ar !== this.props.unit.title_ar
        || form.title_en !== this.props.unit.title_en
        || form.body_en !== this.props.unit.body_en
        || form.body_ar !== this.props.unit.body_ar
        || form.unit_type !== this.props.unit.unit_type
        || form.unit_class !== this.props.unit.unit_class.toUpperCase()
        || form.total_area !== this.props.unit.total_area
        || form.city !== this.props.unit.city.id
        || form.available_as !== this.props.unit.available_as
        || form.bed_type !== this.props.unit.bed_type
        || form.number_of_guests !== this.props.unit.number_of_guests
        || form.number_of_rooms !== this.props.unit.number_of_rooms
        || form.number_of_beds !== this.props.unit.number_of_beds
        || form.number_of_baths !== this.props.unit.number_of_baths
        || this.props.unit.rules.map(rule => (form.rules) ? form.rules.includes(rule.id) : false).includes(false)
        || this.props.unit.amenities.map(am => (form.amenities) ? form.amenities.includes(am.id) : false).includes(false)
      )
      {
        this.setState({displayMessage: true});
      }
      else {
        this.setState({displayMessage: false});
      }
    }
  }



  handleNext = () => {
    const {stepIndex} = this.state;
    if (stepIndex < 5) {
      this.setState({stepIndex: stepIndex + 1});
    }
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  handelFormErrors(errors){
    errors.map(err => {
      this.fields.map(stepFields => {
        if(stepFields.fields.includes(err.props.name)){
          const step = stepFields.step - 1
          this.setState({stepIndex: step})
        }

      })

    })
  }


  // render
  render(){

    const {stepIndex, form} = this.state;
    const contentStyle = {margin: '0 16px'};
    const {unit, users, cities, rules, amenities, formType} = this.props

    const st1 = (
                    form.title_ar
                    && form.title_en
                    && form.body_en
                    && form.body_ar
                  ) ? true : false
    const st2 = (
                    form.price
                    && form.unit_type
                    && form.unit_class
                    && form.total_area
                  ) ? true : false

    const st3 = form.city ? true : false

    const st4 = (
                    form.available_as
                    && (form.bed_type || form.bed_type === 0)
                    && (form.number_of_guests || form.number_of_guests === 0)
                    && (form.number_of_rooms || form.number_of_rooms === 0)
                    && (form.number_of_beds || form.number_of_beds === 0)
                    && (form.number_of_baths || form.number_of_baths === 0)
                  ) ? true : false

    const st5 = form.rules ? true : false

    const canSubmit = (
      st1 && st2 && st3 && st4 && st5
    ) ? false : true

    const nextBtnActive = {
      0 : st1,
      1 : st2,
      2 : st3,
      3 : st4,
      4 : st5,
    }

    return(
      <ValidatorForm
                  ref="form"
                  onSubmit={this.props.handleSubmit}
                  onError={errors => this.handelFormErrors(errors)}
                  className="add-unit-form">



      <Stepper linear={false} activeStep={stepIndex}>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 0})}>
            <span style={{color: (st1 ? 'green' : 'red')}}><FormattedMessage id="unit_information" /></span>
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => st1 ? this.setState({stepIndex: 1}) : false}>
            <span style={{color: (st2 ? 'green' : 'red')}}><FormattedMessage id="unit_details" /></span>
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => st2 ? this.setState({stepIndex: 2}) : false}>
            <span style={{color: (st3 ? 'green' : 'red')}}><FormattedMessage id="location" /></span>
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => st3 ? this.setState({stepIndex: 3}) : false}>
            <span style={{color: (st4 ? 'green' : 'red')}}><FormattedMessage id="rooms_details" /></span>
          </StepButton>
        </Step>

        <Step>
          <StepButton onClick={() => st4 ? this.setState({stepIndex: 4}) : false}>
            <span style={{color: (st5 ? 'green' : 'red')}}><FormattedMessage id="rules_and_amenities" /></span>
          </StepButton>
        </Step>

        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 5})}>
              <span style={{color: (this.state.stepIndex == 5 ? 'red' : 'red')}}><FormattedMessage id="images" /></span>
          </StepButton>
        </Step>
      </Stepper>
      <div style={contentStyle}>

        <Step1 formType={this.props.formType} unit={unit} onChange={this.handleFildsValues}  users={users} display={stepIndex === 0 ? true : false} />
        <Step2 formType={this.props.formType} unit={unit} onChange={this.handleFildsValues} display={stepIndex === 1 ? true : false} />
        <Step3 formType={this.props.formType} unit={unit} cities={cities} onChange={this.handleFildsValues} display={stepIndex === 2 ? true : false} />
        <Step4 formType={this.props.formType} unit={unit} onChange={this.handleFildsValues} display={stepIndex === 3 ? true : false} />
        <Step5 formType={this.props.formType} unit={unit} rules={rules} amenities={amenities} onChange={this.handleFildsValues} display={stepIndex === 4 ? true : false} />
        <Step6
            formType={this.props.formType}
            unit={unit}
            rules={rules}
            images={this.props.images}
            handleImages={this.props.handleImages}
            onRemoveImg={this.props.onRemoveImg}
            display={stepIndex === 5 ? true : false}
            displayMessage={this.state.displayMessage}
        />

        <div style={{marginTop: 12}}>
          <FlatButton
            label={<FormattedMessage id="back" /> }
            disabled={stepIndex === 0}
            onTouchTap={this.handlePrev}
            style={{marginRight: 12}}
          />
          {stepIndex < 5 &&

            <RaisedButton
              label={<FormattedMessage id="next" />}
              disabled={!nextBtnActive[stepIndex]}
              primary={true}
              onTouchTap={this.handleNext}
            />}


        </div>


      </div>

      <hr />
      <center>
        <RaisedButton
          label={formType === 'edit' ? <FormattedMessage id="update" /> : <FormattedMessage id="create" />}
          style={{width: '50%'}}
          primary={true}
          disabled={canSubmit}
          type="submit"
        />
      </center>

      </ValidatorForm>
    )
  }

}

export default AddUnitForm
