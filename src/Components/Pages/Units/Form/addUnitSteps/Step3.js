/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator, AutoCompleteValidator} from 'react-material-ui-form-validator'
import AutoComplete from 'material-ui/AutoComplete';

import GoogleMap from './google-map'
//import SimpleMap from './maps'
//import SimpleForm from './mp'

import {FormattedMessage} from 'react-intl';


class Step3 extends Component{

  constructor(props){
    super(props)

    this.state = {
      city: '',
      center : {
        lat: 24.7135517,
        lng: 46.67529569999999,
      },
      zoom: 13,
      form: {},
      address: 'aaa',
    }

    this.handleSelectLatLng = this.handleSelectLatLng.bind(this)
    this.handleURLtoMap = this.handleURLtoMap.bind(this)

    this.handleChangeField = this.handleChangeField.bind(this)

    this.setAdress = this.setAdress.bind(this)
    this.handleOnChangePlace = this.handleOnChangePlace.bind(this)
  }

  handleURLtoMap(e){
    const url = e.target.value
    const location = this.latLngUrl(url)

    if(location){
      this.setState({lat: parseFloat(location.lng), lng: parseFloat(location.lat)})
    }
  }

  setAdress(address){
    const { form } = this.state;
    form['address'] = address
    this.setState({form})
  }

  handleChangeField(event){

    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })

  }


  handleSelectLatLng(position){
    this.setState({lat: position.lat, lng: position.lng})
  }

  handleOnChangePlace(lat, lng){

    this.setState({center: {
      lat,
      lng
    }})

  }

  componentWillMount(){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({
        city: unit.city.id,
        searchText: unit.city.name,
        center : {
          lat: parseFloat(unit.latitude),
          lng: parseFloat(unit.longitude),
        },
        form: {
          address: unit.address
        },
        zoom: 17,
      })
      this.props.onChange('city', unit.city.id)
    }
  }

  handleSelectCity  = (city) => {
    this.setState({city: city.id, searchText: city.name})
    this.props.onChange('city', city.id)
  }

  render(){
    const {display, cities} = this.props
    const {form} = this.state
    return(
      <div style={{visibility: (display ? 'visible' : 'hidden'), height: (display ? 'auto' : '0px')}}>

        {cities &&
        <div className="form-field">

          <AutoCompleteValidator
            floatingLabelText={<FormattedMessage id="city" />}
            filter={AutoComplete.caseInsensitiveFilter}
            dataSource={cities}
            dataSourceConfig={ {text: 'name', value: 'id'}  }
            value={this.state.city}
            searchText={this.state.searchText}
            openOnFocus={true}
            fullWidth={true}
            onNewRequest={this.handleSelectCity}
            name="city"
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />

           <input style={{display: 'none'}} type="text" name="city_id" value={this.state.city} />
        </div> }

        <div className="form-field">

          <TextValidator
              floatingLabelText={<FormattedMessage id="address" />}
              onChange={this.handleChangeField}
              name="address"
              value={form.address}
              style={{width: '100%'}}

            />
        </div>



        <div className="form-field">
          <GoogleMap
            onChangePlace={this.handleOnChangePlace}
            setAdress={this.setAdress}
            center={this.state.center}
            zoom={this.state.zoom}
            />

            <input type="hidden" name="latitude" value={this.state.center.lat} />
            <input type="hidden" name="longitude" value={this.state.center.lng}   />
        </div>

      </div>

    )
  }
}

export default Step3
