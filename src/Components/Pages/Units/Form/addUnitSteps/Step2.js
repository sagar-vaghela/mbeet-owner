/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'

import {FormattedMessage} from 'react-intl';

class Step2 extends Component{
  constructor(props){
    super(props)

    this.state = {
      loading: false,
      nos: 1,
      sub_unit: null,
      form: {
        sub_unit: 0,
      },
    }

    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleSelectUnitType  = (event, index, value) => {
    this.setState({unit_type: value})
    this.props.onChange('unit_type', value)
  }
  handleSelectUnitClass  = (event, index, value) => {
    this.setState({unit_class: value})
    this.props.onChange('unit_class', value)
  }

  handleSelectSubUnit = (event, index, value) => {
    this.setState({sub_unit: value})
    this.props.onChange('sub_unit', value)
  }


  handleChangeField = (event) => {
    const { form } = this.state;
    form[event.target.id] = event.target.value
    this.setState({ form })
    this.props.onChange(event.target.id, event.target.value)
  }

  componentWillMount(){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({
        unit_type: unit.unit_type,
        unit_class: unit.unit_class.toUpperCase(),
        nos: unit.number_of_subunits,
        form: {
          sub_unit: false,
          price: unit.price,
          total_area: unit.total_area,
        },
      })
      this.props.onChange('unit_type', unit.unit_type)
      this.props.onChange('unit_class', unit.unit_class.toUpperCase())
      this.props.onChange('sub_unit', unit.number_of_subunits)
      this.props.onChange('price', unit.price)
      this.props.onChange('total_area', unit.total_area)
    }
  }

  subunitsShow = () => {
    const {form, nos} = this.state
    if(!form.sub_unit) return nos - 1

    return form.sub_unit
  }

  subunitsSend = () => {
    const {form, nos}    = this.state

    if(!form.sub_unit) return (nos === 1) ? 0 : nos

    if(parseInt(form.sub_unit) === 0) return 0

    return parseInt(form.sub_unit) + 1

  }

  render(){
    const {display} = this.props
    const {form} = this.state

    return(
      <div style={{display: (display ? 'block' : 'none')}}>

      {/* Price* */}
      <div className="form-field">

        <TextValidator
            floatingLabelText={<FormattedMessage id="price" />}
            onChange={this.handleChangeField}
            name="price"
            id="price"
            min={0}
            type="number"
            value={form.price}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />
      </div>


      {/* Unit type */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText={<FormattedMessage id="select_unit_type" />}
           value={this.state.unit_type}
           onChange={this.handleSelectUnitType}
           name="unit_type"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={[<FormattedMessage id="this_field_is_required" />]}
         >

           <MenuItem value={1} primaryText={<FormattedMessage id="resort" />} />
           <MenuItem value={2} primaryText={<FormattedMessage id="villa" />} />
           <MenuItem value={3} primaryText={<FormattedMessage id="apartment" />} />
           <MenuItem value={4} primaryText={<FormattedMessage id="shaleeh" />} />
           <MenuItem value={5} primaryText={<FormattedMessage id="private_room" />} />
           <MenuItem value={6} primaryText={<FormattedMessage id="shared_room" />} />
         </SelectValidator>
         <input type="hidden" name="unit_type" value={this.state.unit_type} />
      </div>

      {/* Unit Class */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText={<FormattedMessage id="unit_class" />}
           value={this.state.unit_class}
           onChange={this.handleSelectUnitClass}
           name="unit_class"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={[<FormattedMessage id="this_field_is_required" />]}
         >

           <MenuItem value="A" primaryText={<FormattedMessage id="a" />} />
           <MenuItem value="B" primaryText={<FormattedMessage id="b" />} />
           <MenuItem value="C" primaryText={<FormattedMessage id="c" />} />
         </SelectValidator>
         <input type="hidden" name="unit_class" value={this.state.unit_class} />
      </div>

      {/* Total Area */}
      <div className="form-field">

        <TextValidator
            floatingLabelText={<FormattedMessage id="total_area" />}
            onChange={this.handleChangeField}
            id="total_area"
            name="total_area"
            value={form.total_area}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />
      </div>

      <div className="form-field">
        <TextValidator
            floatingLabelText={<FormattedMessage id="number_of_subunits" />}
            onChange={this.handleChangeField}
            name="sub_unit_fake"
            id="sub_unit"
            type="number"
            min="0"
            value={this.subunitsShow()}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />
          <input type="hidden" name="number_of_subunits" value={this.subunitsSend()}
          />
      </div>


      </div>
    )
  }

}

export default Step2
