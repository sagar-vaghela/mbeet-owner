import React from 'react';
import GoogleMapReact from 'google-map-react';

import People             from 'material-ui/svg-icons/maps/place'


const AnyReactComponent = ({ text }) => (
  <People style={{
    position: 'relative', color: '#fe6577',
    height: 60, width: 80, top: -55, left: -40,
  }} />

);



class SimpleMap extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      center: {
        lat: this.props.lat,
        lng: this.props.lng,
      },
      zoom: (this.props.zoom) ? this.props.zoom : 11,
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e){
    this.props.onChangePosition(e)
  }


  render() {

    return (
       <GoogleMapReact
        bootstrapURLKeys={{key: "AIzaSyA-CYqpKC-iuXw28jDh7Ac4293PbLKPEQQ"}}
        defaultCenter={{lat: this.props.lat, lng: this.props.lng}}
        center={{lat: this.props.lat, lng: this.props.lng}}
        defaultZoom={this.state.zoom}
        onClick={this.handleClick}
      >
        <AnyReactComponent
          lat={this.props.lat}
          lng={this.props.lng}
        />
      </GoogleMapReact>
    );
  }
}

export default SimpleMap
