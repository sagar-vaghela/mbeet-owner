/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import RaisedButton from 'material-ui/RaisedButton';

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator, AutoCompleteValidator} from 'react-material-ui-form-validator'

import AutoComplete from 'material-ui/AutoComplete';

import SimpleMap from './maps'

import {FormattedMessage} from 'react-intl';

class Step1 extends Component{

  constructor(props){
    super(props)
    this.state = {
      user_id: '',
      searchText: '',
      form: {},
    }

    this.handleChangeField = this.handleChangeField.bind(this)
    this.handleChangeUserID = this.handleChangeUserID.bind(this)
  }
  handleChangeUserID(user){

    this.setState({user_id: user.id, searchText: user.name});
    this.props.onChange('user_id', user.id)
  }

  handleChangeUser = (s, dataSource) => {
    const obj = dataSource.filter(d => d.name === s)[0]
    if(obj) this.setState({user_id: obj.id})
    else this.setState({user_id: ''})
    this.props.onChange('user_id', null)
  }

  handleChangeField(event){

    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
    this.props.onChange(event.target.name, event.target.value)

  }

  componentWillMount(){

    const {formType, unit} = this.props

    if(formType === 'edit'){
      this.setState({
        user_id: unit.owner.user_id,
        searchText: unit.owner.name,
        form: {
          title_en: unit.title_en,
          title_ar: unit.title_ar,
          body_en: unit.body_en,
          body_ar: unit.body_ar,
        }
      })
      this.props.onChange('user_id', unit.owner.user_id)
      this.props.onChange('title_en', unit.title_en)
      this.props.onChange('title_ar', unit.title_ar)
      this.props.onChange('body_en', unit.body_en)
      this.props.onChange('body_ar', unit.body_ar)
    }
  }

  render(){
    const {users, display, unit} = this.props

    const {form} = this.state
    return(
      <div style={{display: (display ? 'block' : 'none')}}>
        <input  name="user_id" value={sessionStorage.getItem('USERID')} type="hidden" className="hidden-input" />
        {/* Title English */}
        <div className="form-field">

         <TextValidator
             floatingLabelText={<FormattedMessage id="title_en" />}
             onChange={this.handleChangeField}
             name="title_en"
             value={form.title_en}
             style={{width: '100%'}}
             validators={['required']}
             errorMessages={[<FormattedMessage id="this_field_is_required" />]}
           />

       </div>

         {/* Title Arabic */}
         <div className="form-field">

           <TextValidator
               floatingLabelText={<FormattedMessage id="title_ar" />}
               onChange={this.handleChangeField}
               name="title_ar"
               value={form.title_ar}
               style={{width: '100%'}}
               validators={['required']}
               errorMessages={[<FormattedMessage id="this_field_is_required" />]}
             />
          </div>

        {/* Description English*/}
        <div className="form-field">

        <TextValidator
            floatingLabelText={<FormattedMessage id="des_en" />}
            onChange={this.handleChangeField}
            name="body_en"
            value={form.body_en}
            multiLine={true}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />

       </div>

        {/* Description Arabic */}
        <div className="form-field">

        <TextValidator
            floatingLabelText={<FormattedMessage id="des_ar" />}
            onChange={this.handleChangeField}
            name="body_ar"
            value={form.body_ar}
            multiLine={true}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />]}
          />

        </div>

      </div>
    )
  }
}
export default Step1
