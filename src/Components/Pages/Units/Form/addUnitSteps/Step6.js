/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import AddImgBtn from '../addUnitSteps/AddImgBtn'
import {imagePath, pathToUrl, filePreview} from '../../../../../Utilities/cloudinaryUpload'
import {Loading} from '../../../../GlobalComponents/GlobalComponents'
import TextField from 'material-ui/TextField';

// External Packages
import $ from 'jquery'

class Step6 extends Component{
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      unit_images: [],

    }
    this.handleUploadImgs = this.handleUploadImgs.bind(this)
    this.onRemoveNewImg = this.onRemoveNewImg.bind(this)
  }

  onRemoveNewImg(index){
    const {handleImages} = this.props
    const images = this.props.images
    images.splice(index, 1)
    handleImages(images)
    this.forceUpdate()
  }



  handleUploadImgs(e){

    let input = e.target
    const files = input.files // get files
    const {handleImages} = this.props // fo setState on EditUnit Component

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false

    this.setState({loading: true}) // Start Loading

    let images = this.props.images // get images array from EditUnit Component

    filePreview(files)
    .then(imgs => {
      this.setState({loading: false}) // Stop Loading
      imgs.map(img => {
        images.push(img)
        handleImages(images)
        this.forceUpdate()
      })

      input.value = null // reset file input
    })
  }

  componentWillMount(){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({unit_images: unit.images})
    }
  }

  componentWillReceiveProps(nextProps){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({unit_images: nextProps.unit.images})
    }
  }




  render(){
    const {display, unit, images, displayMessage} = this.props

    return (
      <div style={{display: (display ? 'block' : 'none')}}>
        {this.state.loading && <Loading />}
        <div className="container" style={{padding: '50px 0'}}>
          <div className="row">
            {this.state.unit_images.map((img, index) => {
              return (
                <div key={'img'+index} className="col-lg-3 col-md-3 col-sm-4 col-image-preview">

                  <span className="delete-prev-img" onClick={() => this.props.onRemoveImg(img.id)}>x</span>
                  <a href={img.url} target="_blank">
                    <img src={img.url} className="rounded img-fluid preview-img" />
                  </a>
                </div>
              )
            })}

            {images.map((img, index) => {
              return (
                <div key={'img'+index} className="col-lg-3 col-md-3 col-sm-4 col-image-preview">
                  <span className="delete-prev-img" onClick={() => this.onRemoveNewImg(index)}>x</span>
                  <img src={img} className="rounded img-fluid preview-img" />
                </div>
              )
            })}

            <div className="col-lg-3 col-md-3 col-sm-4">
              <AddImgBtn onSelectImg={this.handleUploadImgs} />
            </div>
          </div>
        </div>

        {unit &&
          displayMessage &&
          <div className="form-field">
            <TextField
             floatingLabelText="Reason Message"
             style={{width: '100%'}}
             name="message"
             defaultValue={unit.message}
           />
        </div>}

      </div>
    )
  }

}

export default Step6
