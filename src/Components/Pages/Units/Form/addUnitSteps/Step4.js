/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'

import {FormattedMessage} from 'react-intl';

class Step4 extends Component{

  constructor(props){
    super(props)
    this.state = {
      available_as: '',
      bed_type: '',
      form: {}
    }

    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleSelectAvailbleAs  = (event, index, value) => {
    this.setState({available_as: value})
    this.props.onChange('available_as', value)
  }
  handleSelectBedType     = (event, index, value) => {
    this.setState({bed_type: value})
    this.props.onChange('bed_type', value)
  }
  handleChangeField(event){

    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
    this.props.onChange(event.target.name, event.target.value)

  }

  componentWillMount(){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({
        available_as: unit.available_as,
        bed_type: unit.bed_type,
        form: {
          number_of_guests: unit.number_of_guests,
          number_of_rooms: unit.number_of_rooms,
          number_of_beds: unit.number_of_beds,
          number_of_baths: unit.number_of_baths,
        }
      })
      this.props.onChange('available_as', unit.available_as)
      this.props.onChange('bed_type', unit.bed_type)
      this.props.onChange('number_of_guests', unit.number_of_guests)
      this.props.onChange('number_of_rooms', unit.number_of_rooms)
      this.props.onChange('number_of_beds', unit.number_of_beds)
      this.props.onChange('number_of_baths', unit.number_of_baths)
    }
  }

  render(){
    const {display} = this.props
    const {form} = this.state
    return(
      <div style={{display: (display ? 'block' : 'none')}}>
      {/* Unit Available As */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText={<FormattedMessage id="unit_available_as" />}
           value={this.state.available_as}
           onChange={this.handleSelectAvailbleAs}
           name="available_as"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={[<FormattedMessage id="this_field_is_required" />]}
         >
           <MenuItem value={1} primaryText={<FormattedMessage id="daily" />} />
           <MenuItem value={2} primaryText={<FormattedMessage id="weekly" />} />
           <MenuItem value={3} primaryText={<FormattedMessage id="monthly" />} />
           <MenuItem value={4} primaryText={<FormattedMessage id="max_3_months" />} />
         </SelectValidator>
         <input type="hidden" name="available_as" value={this.state.available_as} />
      </div>


      {/* Number of Guests */}
      <div className="form-field">
        <TextValidator
            floatingLabelText={<FormattedMessage id="number_of_guests" />}
            onChange={this.handleChangeField}
            name="number_of_guests"
            value={form.number_of_guests}
            style={{width: '100%'}}
            type="number"
            min={0}
            validators={['required', 'minNumber: 0']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />, <FormattedMessage id="number_of_guests_error" />]}


          />

      </div>

      {/* Number of Rooms */}
      <div className="form-field">

        <TextValidator
            floatingLabelText={<FormattedMessage id="number_of_living_rooms" />}
            onChange={this.handleChangeField}
            name="number_of_rooms"
            value={form.number_of_rooms}
            style={{width: '100%'}}
            type="number"
            min={0}
            validators={['required', 'minNumber: 0']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />, <FormattedMessage id="number_of_living_rooms_error" />]}
          />


      </div>

      {/* Number of Beds */}
      <div className="form-field">

      <TextValidator
          floatingLabelText={<FormattedMessage id="number_of_beds" />}
          onChange={this.handleChangeField}
          name="number_of_beds"
          value={form.number_of_beds}
          style={{width: '100%'}}
          type="number"
          min={0}
          validators={['required', 'minNumber: 0']}
          errorMessages={[<FormattedMessage id="this_field_is_required" />, <FormattedMessage id="number_of_beds_error" />]}
        />

      </div>

      {/* Number of Baths */}
      <div className="form-field">

        <TextValidator
            floatingLabelText={<FormattedMessage id="number_of_baths" />}
            onChange={this.handleChangeField}
            name="number_of_baths"
            value={form.number_of_baths}
            style={{width: '100%'}}
            type="number"
            min={0}
            validators={['required', 'minNumber: 0']}
            errorMessages={[<FormattedMessage id="this_field_is_required" />, <FormattedMessage id="number_of_Baths_error" />]}
          />

      </div>

      <div className="form-field">
        <SelectValidator
           floatingLabelText={<FormattedMessage id="bed_type" />}
           value={this.state.bed_type}
           onChange={this.handleSelectBedType}
           name="bed_type"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={[<FormattedMessage id="this_field_is_required" />]}
         >
           <MenuItem value={1} primaryText={<FormattedMessage id="single" />} />
           <MenuItem value={2} primaryText={<FormattedMessage id="double" />} />
         </SelectValidator>
         <input type="hidden" name="bed_type" value={this.state.bed_type} />
      </div>


      </div>
    )
  }
}

export default Step4
