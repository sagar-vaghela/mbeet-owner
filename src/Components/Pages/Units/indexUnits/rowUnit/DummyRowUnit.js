import React from 'react'
import Avatar       from 'material-ui/Avatar';

// Style
import './rowUnitStyle.css'


const RowUnit = (props) => {

  return (
    <a href="#" className="col-lg-3 unit-item-container dummy">

      <div className="card unit-item">


        <img  className="card-img-top img-fluid"
              src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
              alt="Card image cap" />

        <div className="card-block">

          <div className="unit-owner">

            <Avatar size={50}
                    src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
                    className="owner-avatar"
                    style={{borderColor: '#fff'}}/>

            <span className="owner-name">
              <span style={{background: '#fff', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
            </span>

          </div>

          <h6 className="card-title unit-title">
            <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
          </h6>

          <p className="card-text unit-address">
            <small>
              <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
            </small>
          </p>





          <div className="col-lg-12 unit-details">
            <div className="row">
              <div className="col-lg-6 col-md-6 unit-details-item">
                <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
              </div>

              <div className="col-lg-6 col-md-6 unit-details-item">
                <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
              </div>

              <div className="col-lg-6 col-md-6 unit-details-item">
                <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
              </div>

              <div className="col-lg-6 col-md-6 unit-details-item">
                <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </a>
  )
}
export default RowUnit
