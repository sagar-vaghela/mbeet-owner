import React from 'react'
import Avatar       from 'material-ui/Avatar';
import { Link }           from 'react-router-dom'
import ReactTooltip from 'react-tooltip'
import {FormattedMessage} from 'react-intl';

// Style
import './rowUnitStyle.css'


const RowUnit = (props) => {
  const {unit, onDelete, onRestore, onDisable, onEnable} = props
  const stars = [1,2,3,4,5]
  const enableBtn = {
    display: unit.units_disabled.length > 0 ? "block" : "none"
  }

  const deleteBtn = {
    display: !unit.soft_delete ? "block" : "none"
  }

  const restoreBtn = {
    display: unit.soft_delete ? "block" : "none"
  }

  const direction = localStorage.LOCALE == 'ar' && "unit-item-container-rtd"
  const direction_i = localStorage.LOCALE == 'ar' && "unit-details-item-rtd"
  const direction_owner_name = localStorage.LOCALE == 'ar' && "owner-name-rtd"

  return (
    <Link to={'/units/show/'+unit.id} className={'col-lg-3 unit-item-container '+(unit.soft_delete ? 'deleted' : null) }>
    {unit.unit_status ? (
      <div className="unit-published">
        <FormattedMessage id="published" />
      </div>
    ):(
      <div className="waiting-for-approv">
        <FormattedMessage id="waiting_for_approve" />
      </div>
    )}

    {unit.number_of_subunits > 1 &&
      <div className="subunits-count">
        <span>{parseInt(unit.number_of_subunits) - 1}</span>
          <FormattedMessage id="subunits" />
      </div>
    }


      {unit.soft_delete &&
      <div className="on-trash">
          <div className="alrt">
            <div className="alert  alert-danger">
              <i className="fa fa-trash-o" aria-hidden="true"></i>
              <FormattedMessage id="on_trash" />
            </div>
          </div>
      </div> }

    <div className="unit-actions">
      <Link to={'/units/edit/'+unit.id} data-tip={localStorage.getItem('LOCALE') == 'en' ? "Edit" : "تصحيح"} className="action">
        <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
      </Link>

      <Link to={'/units/'+unit.id+'/special_price'} data-tip={localStorage.getItem('LOCALE') == 'en' ? "Special Price" : "سعر خاص"} className="action">
        <i className="fa fa-pencil" aria-hidden="true"></i>
      </Link>

      <a style={deleteBtn} href="#" data-tip={localStorage.getItem('LOCALE') == 'en' ? "Move to trash" : "ارسال الى سلة المحذوفات"} className="action" onClick={(e) => onDelete(e, unit.id)}>
        <i className="fa fa-trash-o" aria-hidden="true"></i>
      </a>

      <a style={restoreBtn} href="#" data-tip={localStorage.getItem('LOCALE') == 'en' ? "Restore" : "استعادة"} className="action" title="Restore" onClick={(e) => onRestore(e, unit.id)}>
        <i className="fa fa-reply" aria-hidden="true"></i>
      </a>

      {unit.number_of_subunits > 0 &&
        <span>
          <a href="#" data-tip={localStorage.getItem('LOCALE') == 'en' ? "Disable Sub-units" : "تعطيل الوحدات الفرعية"} className="action" onClick={(e) => onDisable(e, unit.id, unit.number_of_subunits)}>
            <i className="fa fa-ban" aria-hidden="true"></i>
          </a>
          <a style={enableBtn} href="#" data-tip={localStorage.getItem('LOCALE') == 'en' ? "Enable Sub-units" : "تمكين الوحدات الفرعية"} className="action" onClick={(e) => onEnable(e, unit.id, unit.number_of_subunits)}>
            <i className="fa fa-power-off" aria-hidden="true"></i>
          </a>
        </span>
      }

      <ReactTooltip place="left" type="dark" effect="solid" />

    </div>
      <div className="card unit-item">

        <div className="card-img-top" style={{backgroundImage: `url(${unit.default_image})`}}>
          <img  className="img-fluid"
                src={(unit.images.length > 0) ? unit.images[0].url : unit.default_image}
                alt=""

          />
        </div>

        <div className="card-block">

          <div className="unit-owner">

            <Avatar size={50}
                    src="images/user-avatar.png"
                    className="owner-avatar"/>

                  <span className={"owner-name "+ direction_owner_name}>
              {unit.owner.name}
            </span>

          </div>

          <h6 className="card-title unit-title">
            {unit.title_en}
          </h6>

          <p className="card-text unit-address">
            <small>{unit.address}</small>
          </p>

          <p className="card-text unit-rating">
            {stars.map(star => {
              const starClass = (star <= unit.avg_star) ? 'fa fa-star' : 'fa fa-star-o'
              return (<i key={'star'+star} className={starClass} aria-hidden="true"></i>)
            })}
          </p>
          <div className="unit-price">
            SAR {unit.price}
          </div>



          <div className="col-lg-12 unit-details">
            <div className="row">
              <div className={"col-lg-6 col-md-6 unit-details-item "+ direction_i}>
                <i className="mdi mdi-account-multiple"></i>
                {unit.number_of_guests} <FormattedMessage id="guests" />
              </div>

              <div className={"col-lg-6 col-md-6 unit-details-item "+ direction_i}>
                <i className="mdi mdi-hotel" aria-hidden="true"></i>
                {unit.number_of_beds} <FormattedMessage id="beds" />
              </div>

              <div className={"col-lg-6 col-md-6 unit-details-item "+ direction_i}>
                <i className="fa fa-home"></i>
                {unit.number_of_rooms} <FormattedMessage id="rooms" />
              </div>

              <div className={"col-lg-6 col-md-6 unit-details-item "+ direction_i}>
                <i className="fa fa-bath" aria-hidden="true"></i>
                {unit.number_of_baths} <FormattedMessage id="baths" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}
export default RowUnit
