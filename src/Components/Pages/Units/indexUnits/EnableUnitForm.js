import React, {Component} from 'react'
import {connect} from 'react-redux'


import { TextValidator, ValidatorForm, SelectValidator, DateValidator} from 'react-material-ui-form-validator'
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { CSSTransitionGroup } from 'react-transition-group'
import dateFormat  from 'dateformat'
import FlatButton from 'material-ui/FlatButton';

import {Loading, AlertMsg} from '../../../GlobalComponents/GlobalComponents'
import {enableUnit, clearMsg} from '../../../../Redux/actions/unitsActions'

import {FormattedMessage} from 'react-intl';

class EnableUnitForm extends Component{
  subunits = this.props.subunits
  state = {
    form: {
      sub_unit: this.subunits <= 1 ? 0 : 1
    },
    status: 0,
  }

  handleOnChangeFrom = (a, data) => this.setState({date_from: data})
  handleOnChangeTo    = (a, data) => this.setState({date_to: data})

  handleSubmit = (e) => {
    e.preventDefault()
    const {dispatch, unit_id} = this.props
    var formData = new FormData(e.target)

    this.setState({loading: true})
    dispatch(enableUnit(unit_id, formData))
    .then(() => this.setState({status: 1}))
    .catch(() => this.setState({loading: false}))

  }
  closeModal = (e) => {
    const {status} = this.state

    if(e.target.id != 'parent') return false
    const {obj} = this.props
    obj.setState({enable_unit: false})

    if(status === 1)
      this.props.reload()
  }

  renderSubunits = (count) => {
    let result = []

    for(let i = 1; i <= count ; i++){
      result.push( <MenuItem
                        key={'subu'+i}
                        value={i}
                        primaryText={i}
                    />
                  )
    }

    return result

  }

  handleChangeField = (event) => {
    const { form } = this.state;
    form[event.target.id] = event.target.value
    this.setState({ form })
  }

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(clearMsg())
  }

  render(){
    const {date_to, date_from, sub_unit, loading, form} = this.state
    const {unit_id, subunits, succMsg, errMsg} = this.props

    const minNumber = parseInt(form.sub_unit) > 0 ? 1 : 0

    const dateFrom  =   date_from
                        ? dateFormat(new Date(date_from), 'yyyy-mm-dd 12:00:00').toString()
                        : ''

    const dateTo    =   date_to
                        ? dateFormat(new Date(date_to), 'yyyy-mm-dd 12:00:00').toString()
                        : ''

    const TrassiosnOptions = {
      transitionName: 'modal',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }
    return (
      <div className="popup-modal" onClick={this.closeModal} id="parent">
        <CSSTransitionGroup {...TrassiosnOptions}>
        <div className="modal-content">
          {loading && <Loading success={succMsg} />}
          <h2><FormattedMessage id="enable" /> <FormattedMessage id="unit" /></h2>
          <hr style={{width: '100%', margin: '0'}} />
          <ValidatorForm onSubmit={this.handleSubmit}>

          <div className="form-field">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-sm-6">
                <DateValidator
                  floatingLabelText={<FormattedMessage id="from" />}
                  name="from"
                  mode="landscape"
                  minDate={new Date()}
                  value={date_from ? date_from : null}
                  onChange={this.handleOnChangeFrom}
                  mode="landscape"
                  container="inline"
                  validators={['required']}
                  errorMessages={[<FormattedMessage id="this_field_is_required" />]}
                />
              <input type="hidden" name="start_date" value={dateFrom? dateFrom : ''} />
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6">
                <DateValidator
                  floatingLabelText={<FormattedMessage id="to" />}
                  container="inline"
                  name="to"
                  disabled={!date_from}
                  minDate={date_from}
                  mode="landscape"
                  value={date_to ? date_to : null}
                  onChange={this.handleOnChangeTo}
                  validators={['required']}
                  errorMessages={[<FormattedMessage id="this_field_is_required" />]}
                />
              <input type="hidden" name="end_date" value={dateTo? dateTo : ''} />
              </div>
            </div>

          </div>

          <div className="form-field">
            <TextValidator
                floatingLabelText={<FormattedMessage id="number_of_subunits" />}
                onChange={this.handleChangeField}
                name="number_of_disabled_units"
                id="sub_unit"
                type="number"
                min={parseInt(minNumber)}
                max={parseInt(subunits)}
                value={form.sub_unit? parseInt(form.sub_unit) : ''}
                style={{width: '100%'}}
                validators={["required", "maxNumber:"+(subunits)+"", "minNumber:"+minNumber+""]}
                errorMessages={[<FormattedMessage id="this_field_is_required" />, <FormattedMessage id="disable_unit_error1" /> +(subunits), <FormattedMessage id="disable_unit_error2" />+minNumber]}
              />

          </div>



          <div className="form-field">
            <FlatButton
              label={<FormattedMessage id="cancel" />}
              onTouchTap={() => this.props.obj.setState({enable_unit: false})}
              style={{marginRight: 12}}
            />
            <RaisedButton
              label={<FormattedMessage id="submit" />}

              primary={true}
              type="submit"
            />
          </div>

          </ValidatorForm>
          {(errMsg) && <AlertMsg error={errMsg} />}
        </div>
        </CSSTransitionGroup>

      </div>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    errMsg: store.units.enableErrMsg,
    succMsg: store.units.succMsg,
    fetching: store.units.fetching,
  }
}

EnableUnitForm = connect(mapStateToProps)(EnableUnitForm)
export default EnableUnitForm
