import React from 'react'
import {FormattedMessage} from 'react-intl';

const Privacy = () => (
  <div>
    <div className="container">
      <div className="single-page-content">

        <div className="page-container">
          <h1 className="page-title"><FormattedMessage id="privacy" /></h1>
          <p><FormattedMessage id="privacy_p" /></p>
        </div>
      </div>
    </div>
  </div>
)
export default Privacy
