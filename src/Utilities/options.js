
/*****************************
*   Unit Options
*****************************/
export const unitType = [
  null,
  'resort',
  'villa',
  'apartment',
  'shaleeh',
  'private room',
  'hared room',
]

export const availableAs = [
  null,
  'Daily',
  'Weekly',
  'Monthly',
  '+3 Months',
]

/*****************************
*   Bookint Options
******************************/

// Payment Status
export const paymentsStatus = [
  null,
  'pending',
  'cancelled',
  'confirmed',
  'completed',
]

// Lease Status
export const leaseStatus = [
  null,
  'inactive',
  'expired',
  'renewed',
  'confirmed',
  'cancelled',
]
