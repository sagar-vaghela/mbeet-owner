import swal from 'sweetalert'
import './sweetAlertStyle.css'
/*
* handleResponseErr - to get the error message when fetch the API
* Return: (String) - error message
*
* @err : (Object) - error object
*/
export function handleResponseErr(err){

  // check if there any response from the server
  const response = (typeof err.response !== 'undefined') ? err.response : false

  // check if the server send any data
  const resData = response.hasOwnProperty('data') ? response.data : false

  // if the server not return any response put browser error
  const error = !resData ? err.message : false

  // the result error message
  const errMesg = (error) ? error : (resData.message) ? resData.message : resData.error

  switch (errMesg) {
    case 'Not Found':
      return localStorage.getItem('LOCALE') === 'en' ? 'Request wrong API' : 'طلب أبي خاطئ'

    case 'Network Error':
      return localStorage.getItem('LOCALE') === 'en' ? "Can't send request to the server, please check your internet connection!" : "لا يمكن إرسال طلب إلى الخادم، يرجى التحقق من اتصال الإنترنت الخاص بك!"

    default :
      return errMesg

  }

}


export function deleteItem(id, dispatch, deleteAction, errMsg, obj = null, btnMsg = 'Yes, delete it!', title = 'Deleted'){
  swal({
    title: localStorage.getItem('LOCALE') === 'en' ? "Are you sure?" : "هل أنت واثق؟",
    type: "warning",
    showCancelButton: true,
    cancelButtonText: localStorage.getItem('LOCALE') === 'en' ? "Cancel" : "إلغاء",
    confirmButtonColor: "#DD6B55",
    confirmButtonText: localStorage.getItem('LOCALE') === 'en' ? btnMsg : "نعم، حذفه!",
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
  },
  () => {

    dispatch(deleteAction(id)).then(() => {
      swal({
        title: title,
        type: 'success',
        timer: 2000,
        showConfirmButton: false
      })
      obj.forceUpdate()
    }).catch(() => {
      swal("Error", errMsg, "error");
    })
  })
}

/*
* getDataLatLng: to get the Lat & Lng from Google Map URL
*
* @url: (String) | Google Map URL
*/
export function getLatLngFromData(url){

  // url is required
  if(!url) return false

  // get data parameter
  const regex = new RegExp('data=(.*)');
  const lon_lat_match = url.match(regex);


  // if there is no data paramter
  if(!lon_lat_match)  return false

  // take data value only, no need for other values
  const dataValue = lon_lat_match[1].split('?')

  // convert data value to array
  const data = dataValue[0].split("!");

  // get Lat
  const center = {lat: null, lng: null}

  data.map(value => {
    if(value.substring(0,2) === '3d')
      center.lat = value.toString().replace('3d','')

    if(value.substring(0,2) === '4d')
      center.lng = value.toString().replace('4d','')

  })

  if(!center.lat || !center.lng) return false

  const lat = center.lat.match(new RegExp(/[a-zA-Z]/i))
  if(lat) return false

  const lng = center.lng.match(new RegExp(/[a-zA-Z]/i))
  if(lng) return false



  return ({lat: parseFloat(center.lat), lng: parseFloat(center.lng)})

}
