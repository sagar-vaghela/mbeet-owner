/*
* App: the main component
*
*/

/********************
*   Import Packages
*********************/

// Main Packages
import React, { Component } from 'react'; // React & React component
import {connect} from 'react-redux'

import { HashRouter as Router } from "react-router-dom"

// Components
import MaterialUiProvider from './Theme/MaterialUiProvider'
import RootRouters from './Routers/RootRouters'

// Style
import './Theme/style.css'
import './Theme/style-ar.css'

// External Packages
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import 'mdi/css/materialdesignicons.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'sweetalert/dist/sweetalert.min.js'
import 'sweetalert/dist/sweetalert.css'
import "video-react/dist/video-react.css";

import {IntlProvider , addLocaleData} from 'react-intl';
// import {addLocaleData} from 'react-intl';
import en from 'react-intl/locale-data/en';
import ar from 'react-intl/locale-data/ar';

// import messages from './messages'
import messages from './Utilities/messages'

import {setLocale} from './Redux/actions/setLocaleAction'

import DirectionProvider, { DIRECTIONS } from 'react-with-direction/dist/DirectionProvider';

addLocaleData([...en,...ar])

class App extends Component {

  componentWillMount(){
    const {dispatch} = this.props;
    if (localStorage.LOCALE === "undefined")
    {
      dispatch(setLocale('en'))
    }
    else {
      dispatch(setLocale(localStorage.LOCALE))
    }
  }

  render() {
    const {LOCALE} = this.props;
    const message = LOCALE === 'en' ? messages.en : messages.ar;
    return (
      <DirectionProvider direction={localStorage.LOCALE === "en" || localStorage.LOCALE === "undefined" ? DIRECTIONS.LTR : DIRECTIONS.RTL} >
        <IntlProvider locale={LOCALE ? LOCALE : 'en'} messages={message}>
          <MaterialUiProvider>
            <Router>
                <RootRouters />
            </Router>
          </MaterialUiProvider>
        </IntlProvider>
      </DirectionProvider>

    );
  }
}

const mapStateToProps = (store) => {
  return {
    LOCALE: store.setLocale.LOCALE
  }
}

const app = connect(mapStateToProps)(App);

export default app;
